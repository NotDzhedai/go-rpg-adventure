package ui

import (
	"gitlab.com/NotDzhedai/rpg/game"
)

type dialogState struct{}

func newDialogState() state {
	return &dialogState{}
}

func (sm *dialogState) enter(ui *ui, params ...interface{}) {
	npc, ok := params[0].(*game.NPC)
	if !ok {
		panic("NPC error convert")
	}

	ui.activeNpc = npc
}

func (s *dialogState) update(ui *ui, input *game.Input, level *game.Level) {
	ui.renderer.Clear()

	ui.drawDialog(ui.activeNpc)
	npc := ui.activeNpc
	if ui.activeNpc.IsTradeStarted {
		ui.state.change(uiTrade, ui, npc)
	}
	if !ui.activeNpc.InRange(level.Player.Pos) {
		ui.state.change(uiMain, ui)
	}

	choice := ui.checkOption()
	if choice != nil {
		input.Type = game.MakeChoice
		input.Choice = choice
		input.Npc = ui.activeNpc
	}
}

func (s *dialogState) exit(ui *ui) {
	ui.activeNpc.CloseDialog()
	ui.activeNpc = nil
}

func (s *dialogState) change(state stateType, ui *ui, params ...interface{}) {}
