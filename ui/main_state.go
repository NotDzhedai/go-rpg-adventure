package ui

import (
	"gitlab.com/NotDzhedai/rpg/game"
)

type mainState struct{}

func newMainState() state {
	return &mainState{}
}

func (s *mainState) enter(ui *ui, params ...interface{}) {}

func (s *mainState) exit(ui *ui) {}

func (s *mainState) update(ui *ui, input *game.Input, level *game.Level) {
	ui.drawMain(level)
}

func (s *mainState) change(state stateType, ui *ui, params ...interface{}) {}
