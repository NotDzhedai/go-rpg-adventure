package ui

import (
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/NotDzhedai/rpg/game"
)

type mouseState struct {
	leftButton  bool
	rightButton bool
	pos         game.Pos
}

func getMouseState() *mouseState {
	mouseX, mouseY, mouseButtonState := sdl.GetMouseState()
	leftButton := mouseButtonState & sdl.ButtonLMask()
	rightButton := mouseButtonState & sdl.ButtonRMask()
	var result mouseState
	result.pos = game.Pos{X: int(mouseX), Y: int(mouseY)}
	result.leftButton = !(leftButton == 0)
	result.rightButton = !(rightButton == 0)
	return &result
}
