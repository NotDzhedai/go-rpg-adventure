package ui

import "gitlab.com/NotDzhedai/rpg/game"

type inventoryState struct{}

func newInventoryState() state {
	return &inventoryState{}
}

func (sm *inventoryState) enter(ui *ui, params ...interface{}) {

}

func (sm *inventoryState) update(ui *ui, input *game.Input, level *game.Level) {
	ui.renderer.Clear()

	if ui.draggedItem != nil && !ui.currentMouseState.leftButton && ui.prevMouseState.leftButton {
		// ! Equip item
		item := ui.checkEquippedItem()
		if item != nil {
			input.Type = game.EquipItem
			input.Item = item
			ui.draggedItem = nil
		}
		// ! Drop item
		if ui.draggedItem != nil {
			item := ui.checkDroppedItem()
			if item != nil {
				input.Type = game.DropItem
				input.Item = item
				ui.draggedItem = nil
			}
		}
	}
	// ! drag & drop item
	if !ui.currentMouseState.leftButton || ui.draggedItem == nil {
		ui.draggedItem = ui.checkInventoryItems(level)
	}

	// ui.state.states[uiMain].update(ui, input, level)

	ui.drawInventory(level)
}

func (s *inventoryState) exit(ui *ui) {}

func (s *inventoryState) change(state stateType, ui *ui, params ...interface{}) {}
