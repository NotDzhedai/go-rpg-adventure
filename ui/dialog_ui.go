package ui

import (
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/NotDzhedai/rpg/game"
)

func (ui *ui) drawPopup(npc *game.NPC, npcRect *sdl.Rect) {
	// dialog popup
	dRect := &sdl.Rect{
		X: npcRect.X + 20,
		Y: npcRect.Y - 50,
		W: 100,
		H: 55,
	}
	ui.renderer.Copy(ui.popupTexture, nil, dRect)

	// message
	texture := ui.dialogFontToTexture(
		npc.CurrentMessage,
		sdl.Color{R: 144, G: 0, B: 0, A: 0},
	)
	_, _, w, h, _ := texture.Query()
	ui.renderer.Copy(texture, nil, &sdl.Rect{X: dRect.X + 20, Y: dRect.Y + 3, W: w, H: h})
}

func (ui *ui) drawDialog(npc *game.NPC) {
	dialogRect := ui.getMainCenterRect()
	ui.renderer.Copy(ui.dialogBackground, nil, dialogRect)

	// Name
	npcNameOffsetX := int32(float64(ui.winWidth) * .38)
	npcNameOffsetY := int32(float64(ui.winHeigth) * .24)
	texture := ui.smallFontToTexture(npc.Name, sdl.Color{R: 0, G: 0, B: 0, A: 0})
	_, _, w, h, _ := texture.Query()
	ui.renderer.Copy(texture, nil, &sdl.Rect{X: npcNameOffsetX, Y: npcNameOffsetY, W: w, H: h})

	// Portret
	npcPortretOffsetX := int32(float64(ui.winWidth) * .5)
	npcPortretOffsetY := int32(float64(ui.winHeigth) * .2)
	npcSrcRect := ui.texturesSrc[npc.TileID]
	dstRect := sdl.Rect{
		X: npcPortretOffsetX,
		Y: npcPortretOffsetY,
		W: int32(float64(dialogRect.W) / 2.5),
		H: int32(float64(dialogRect.H) / 2.5),
	}
	ui.renderer.Copy(ui.texturesAtlas, npcSrcRect, &dstRect)

	// Text
	npcTextOffsetX := int32(float64(ui.winWidth) * .35)
	npcTextOffsetY := int32(float64(ui.winHeigth) * .5)
	texture = ui.smallFontToTexture(npc.StoryNode.Text, sdl.Color{R: 0, G: 0, B: 0, A: 0})
	_, _, w, h, _ = texture.Query()
	ui.renderer.Copy(texture, nil, &sdl.Rect{X: npcTextOffsetX, Y: npcTextOffsetY, W: w, H: h})

	// Options
	for i, c := range npc.StoryNode.Choices {
		texture = ui.smallFontToTexture("- "+c.Option, sdl.Color{R: 0, G: 0, B: 0, A: 0})
		_, _, w, h, _ := texture.Query()
		option := ui.getOption(i)
		ui.renderer.Copy(ui.paperBarTexture, nil, option)
		ui.renderer.Copy(texture, nil, &sdl.Rect{X: option.X + 5, Y: option.Y + 5, W: w, H: h})
	}
}

func (ui *ui) getOption(i int) *sdl.Rect {
	npcOptionsOffsetX := int32(float64(ui.winWidth) * .36)
	npcOptionsOffsetY := int32(float64(ui.winHeigth) * .58)

	return &sdl.Rect{X: npcOptionsOffsetX, Y: npcOptionsOffsetY + int32(i)*45, W: 400, H: 40}
}

func (ui *ui) checkOption() *game.Choice {
	if !ui.currentMouseState.leftButton && ui.prevMouseState.leftButton {
		mousePos := ui.currentMouseState.pos
		for i, c := range ui.activeNpc.StoryNode.Choices {
			oRect := ui.getOption(i)
			if oRect.HasIntersection(&sdl.Rect{X: int32(mousePos.X), Y: int32(mousePos.Y), W: 1, H: 1}) {
				return c
			}
		}
	}
	return nil
}
