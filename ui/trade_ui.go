package ui

import (
	"fmt"

	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/NotDzhedai/rpg/game"
)

func (ui *ui) drawPlayerInventory(input *game.Input, level *game.Level) {
	invWidth := int32(float64(ui.winWidth) * .3)
	invHeight := int32(float64(ui.winHeigth) * .8)
	offsetX := int32(float64(ui.winWidth) * .15)
	offsetY := (int32(ui.winHeigth) - invHeight) / 2
	ui.renderer.Copy(ui.inventoryBackground, nil, &sdl.Rect{
		X: offsetX,
		Y: offsetY,
		W: invWidth,
		H: invHeight,
	})

	texture := ui.smallFontToTexture(
		fmt.Sprintf("%s, %d gold", level.Player.Name, level.Player.Gold),
		sdl.Color{R: 0, G: 0, B: 0, A: 0},
	)
	_, _, w, h, _ := texture.Query()
	ui.renderer.Copy(texture, nil, &sdl.Rect{
		X: offsetX + (invWidth / 3),
		Y: offsetY + 20,
		W: w,
		H: h,
	})

	for i, item := range level.Player.Inventory {
		texture = ui.smallFontToTexture(
			fmt.Sprintf("%s - (%d gold)", item.Name, item.Price),
			sdl.Color{R: 0, G: 0, B: 0, A: 0},
		)
		_, _, w, h, _ := texture.Query()
		option := ui.getPlayerItem(i)
		ui.renderer.Copy(ui.paperBarTexture, nil, option)
		ui.renderer.Copy(texture, nil, &sdl.Rect{X: option.X + 5, Y: option.Y + 5, W: w, H: h})
	}
}

func (ui *ui) getPlayerItem(i int) *sdl.Rect {
	npcOptionsOffsetX := int32(float64(ui.winWidth) * .15)
	npcOptionsOffsetY := int32(float64(ui.winHeigth) * .2)

	return &sdl.Rect{X: npcOptionsOffsetX + 10, Y: npcOptionsOffsetY + int32(i)*40, W: 400, H: 40}
}

func (ui *ui) drawTraderInventory(input *game.Input, level *game.Level) {
	invWidth := int32(float64(ui.winWidth) * .3)
	invHeight := int32(float64(ui.winHeigth) * .8)
	offsetX := int32(float64(ui.winWidth) * .6)
	offsetY := (int32(ui.winHeigth) - invHeight) / 2
	ui.renderer.Copy(ui.inventoryBackground, nil, &sdl.Rect{
		X: offsetX,
		Y: offsetY,
		W: invWidth,
		H: invHeight,
	})

	texture := ui.smallFontToTexture(
		fmt.Sprintf("%s inventory, %d gold", ui.activeNpc.Name, ui.activeNpc.Gold),
		sdl.Color{R: 0, G: 0, B: 0, A: 0},
	)
	_, _, w, h, _ := texture.Query()
	ui.renderer.Copy(texture, nil, &sdl.Rect{
		X: offsetX + (invWidth / 5),
		Y: offsetY + 20,
		W: w,
		H: h,
	})

	for i, item := range ui.activeNpc.ItemToSell {
		texture = ui.smallFontToTexture(
			fmt.Sprintf("%s - (%d gold)", item.Name, item.Price),
			sdl.Color{R: 0, G: 0, B: 0, A: 0},
		)
		_, _, w, h, _ := texture.Query()
		option := ui.getTraderItem(i)
		ui.renderer.Copy(ui.paperBarTexture, nil, option)
		ui.renderer.Copy(texture, nil, &sdl.Rect{X: option.X + 5, Y: option.Y + 5, W: w, H: h})
	}
}

func (ui *ui) getTraderItem(i int) *sdl.Rect {
	npcOptionsOffsetX := int32(float64(ui.winWidth) * .6)
	npcOptionsOffsetY := int32(float64(ui.winHeigth) * .2)

	return &sdl.Rect{X: npcOptionsOffsetX + 10, Y: npcOptionsOffsetY + int32(i)*40, W: 400, H: 40}
}

func (ui *ui) checkPlayerItem(level *game.Level) *game.Item {
	if !ui.currentMouseState.leftButton && ui.prevMouseState.leftButton {
		mousePos := ui.currentMouseState.pos
		for i, c := range level.Player.Inventory {
			oRect := ui.getPlayerItem(i)
			if oRect.HasIntersection(&sdl.Rect{X: int32(mousePos.X), Y: int32(mousePos.Y), W: 1, H: 1}) {
				return c
			}
		}
	}
	return nil
}

func (ui *ui) checkTraderItem(level *game.Level) *game.Item {
	if !ui.currentMouseState.leftButton && ui.prevMouseState.leftButton {
		mousePos := ui.currentMouseState.pos
		for i, c := range ui.activeNpc.ItemToSell {
			oRect := ui.getTraderItem(i)
			if oRect.HasIntersection(&sdl.Rect{X: int32(mousePos.X), Y: int32(mousePos.Y), W: 1, H: 1}) {
				return c
			}
		}
	}
	return nil
}
