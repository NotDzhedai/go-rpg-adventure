package ui

import (
	"fmt"
	"math"

	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/NotDzhedai/rpg/game"
)

func (ui *ui) drawMain(level *game.Level) {
	// ! center the camera to player
	if ui.centerX == -1 && ui.centerY == -1 {
		ui.centerX = level.Player.X
		ui.centerY = level.Player.Y
	}

	limit := 5
	if level.Player.X > ui.centerX+limit {
		diff := level.Player.X - (ui.centerX + limit)
		ui.centerX += diff
	} else if level.Player.X < ui.centerX-limit {
		diff := (ui.centerX - limit) - level.Player.X
		ui.centerX -= diff
	} else if level.Player.Y > ui.centerY+limit {
		diff := level.Player.Y - (ui.centerY + limit)
		ui.centerY += diff
	} else if level.Player.Y < ui.centerY-limit {
		diff := (ui.centerY - limit) - level.Player.Y
		ui.centerY -= diff
	}

	offsetX := int32((ui.winWidth / 2) - ui.centerX*TILE_SIZE)
	offsetY := int32((ui.winHeigth / 2) - ui.centerY*TILE_SIZE)

	ui.renderer.Clear()

	// ! Draw main
	for y, row := range level.Map {
		for x, tile := range row {
			// ! get main texture
			srcRectM, okM := ui.texturesSrc[tile.Main]
			// ! get top texture
			srcRectT, okT := ui.texturesSrc[tile.Top]
			if !okM {
				py := int32(math.Floor(float64(tile.Main / 64)))
				px := tile.Main - (py * 64)
				srcRectM = &sdl.Rect{X: px * TILE_SIZE, Y: py * TILE_SIZE, W: TILE_SIZE, H: TILE_SIZE}
				ui.texturesSrc[tile.Main] = srcRectM
			}
			if !okT {
				py := int32(math.Floor(float64(tile.Top / 64)))
				px := tile.Top - (py * 64)
				srcRectT = &sdl.Rect{X: px * TILE_SIZE, Y: py * TILE_SIZE, W: TILE_SIZE, H: TILE_SIZE}
				ui.texturesSrc[tile.Top] = srcRectT
			}
			if tile.Visible || tile.Seen {
				dstRect := sdl.Rect{X: int32(x*TILE_SIZE) + offsetX, Y: int32(y*TILE_SIZE) + offsetY, W: TILE_SIZE, H: TILE_SIZE}
				if tile.Seen && !tile.Visible {
					ui.texturesAtlas.SetColorMod(128, 128, 128)
				} else {
					ui.texturesAtlas.SetColorMod(255, 255, 255)
				}
				// ! copy main
				ui.renderer.CopyEx(ui.texturesAtlas, srcRectM, &dstRect, tile.Angle, nil, tile.Flip)
				// ! copy top
				ui.renderer.CopyEx(ui.texturesAtlas, srcRectT, &dstRect, tile.Angle, nil, tile.Flip)
			}

		}
	}

	// ! reset color mode
	ui.texturesAtlas.SetColorMod(255, 255, 255)
	// ! Draw torches
	for pos, torch := range level.Torches {
		if level.Map[pos.Y][pos.X].Visible {
			dstRect := sdl.Rect{X: int32(pos.X*TILE_SIZE) + offsetX, Y: int32(pos.Y*TILE_SIZE) + offsetY, W: TILE_SIZE, H: TILE_SIZE}
			srcRect, ok := ui.texturesSrc[torch.TileID]
			if !ok {
				py := int32(math.Floor(float64(torch.TileID / 64)))
				px := torch.TileID - (py * 64)
				srcRect = &sdl.Rect{X: px * TILE_SIZE, Y: py * TILE_SIZE, W: TILE_SIZE, H: TILE_SIZE}
				ui.texturesSrc[torch.TileID] = srcRect
			}
			ui.renderer.Copy(ui.texturesAtlas, srcRect, &dstRect)
		}
	}

	// ! Draw monsters
	for pos, monster := range level.Monsters {
		if level.Map[pos.Y][pos.X].Visible {
			dstRect := sdl.Rect{X: int32(pos.X*TILE_SIZE) + offsetX, Y: int32(pos.Y*TILE_SIZE) + offsetY, W: TILE_SIZE, H: TILE_SIZE}
			srcRect, ok := ui.texturesSrc[monster.TileID]
			if !ok {
				py := int32(math.Floor(float64(monster.TileID / 64)))
				px := monster.TileID - (py * 64)
				srcRect = &sdl.Rect{X: px * TILE_SIZE, Y: py * TILE_SIZE, W: TILE_SIZE, H: TILE_SIZE}
				ui.texturesSrc[monster.TileID] = srcRect
			}
			ui.drawHpBarUnderCharacter(&monster.Character, offsetX, offsetY)
			ui.renderer.Copy(ui.texturesAtlas, srcRect, &dstRect)
		}
	}

	// ! Draw npcs
	for pos, npc := range level.NPCs {
		if level.Map[pos.Y][pos.X].Visible {
			dstRect := sdl.Rect{X: int32(pos.X*TILE_SIZE) + offsetX, Y: int32(pos.Y*TILE_SIZE) + offsetY, W: TILE_SIZE, H: TILE_SIZE}
			srcRect, ok := ui.texturesSrc[npc.TileID]
			if !ok {
				py := int32(math.Floor(float64(npc.TileID / 64)))
				px := npc.TileID - (py * 64)
				srcRect = &sdl.Rect{X: px * TILE_SIZE, Y: py * TILE_SIZE, W: TILE_SIZE, H: TILE_SIZE}
				ui.texturesSrc[npc.TileID] = srcRect
			}
			// ! if player in interact zone
			if npc.Status == game.Active {
				ui.renderer.SetDrawColor(227, 194, 77, 50)
				ui.renderer.FillRect(&dstRect)
				// ! npc popup
				ui.drawPopup(npc, &dstRect)
			}
			if npc.IsDialogStarted {
				ui.state.change(uiDialog, ui, npc)
			}
			ui.renderer.Copy(ui.texturesAtlas, srcRect, &dstRect)
		}
	}

	// ! reset rect colors
	ui.renderer.SetDrawColor(0, 0, 0, 0)
	// ! Draw items on ground
	for pos, items := range level.Items {
		tile := level.Map[pos.Y][pos.X]
		if tile.Visible {
			if tile.Top == game.ClosedChest || tile.Top == game.OpenedChest {
				continue
			}
			for _, item := range items {
				dstRect := sdl.Rect{X: int32(pos.X*TILE_SIZE) + offsetX, Y: int32(pos.Y*TILE_SIZE) + offsetY, W: TILE_SIZE, H: TILE_SIZE}
				srcRect, ok := ui.texturesSrc[item.TileID]
				if !ok {
					py := int32(math.Floor(float64(item.TileID / 64)))
					px := item.TileID - (py * 64)
					srcRect = &sdl.Rect{X: px * TILE_SIZE, Y: py * TILE_SIZE, W: TILE_SIZE, H: TILE_SIZE}
					ui.texturesSrc[item.TileID] = srcRect
				}
				ui.renderer.Copy(ui.texturesAtlas, srcRect, &dstRect)
			}
		}
	}

	// ! Draw player
	srcRect, ok := ui.texturesSrc[level.Player.TileID]
	if !ok {
		py := int32(math.Floor(float64(level.Player.TileID / 64)))
		px := level.Player.TileID - (py * 64)
		srcRect = &sdl.Rect{X: px * TILE_SIZE, Y: py * TILE_SIZE, W: TILE_SIZE, H: TILE_SIZE}
		ui.texturesSrc[level.Player.TileID] = srcRect
	}
	dstRect := &sdl.Rect{
		X: int32(level.Player.X*TILE_SIZE) + offsetX,
		Y: int32(level.Player.Y*TILE_SIZE) + offsetY,
		W: TILE_SIZE,
		H: TILE_SIZE}

	// ! Draw hpbar under player
	ui.drawHpBarUnderCharacter(&level.Player.Character, offsetX, offsetY)
	ui.drawPlayer(level, offsetX, offsetY, srcRect, dstRect)

	// ! Items menu on ground
	barStart := int32(float64(ui.winWidth) * .8)
	barWidth := int32(ui.winWidth) - barStart
	ui.renderer.Copy(ui.paperBarTexture, nil, &sdl.Rect{
		X: barStart,
		Y: int32(ui.winHeigth) - TILE_SIZE,
		W: barWidth,
		H: TILE_SIZE,
	})
	items := level.Items[level.Player.Pos]
	for i, item := range items {
		itemSrcRect := ui.texturesSrc[item.TileID]
		ui.renderer.Copy(
			ui.texturesAtlas,
			itemSrcRect,
			ui.getItemPickupMenu(i),
		)
	}

	// ! Draw events
	textStartY := int32(float64(ui.winHeigth) * 0.85)
	_, fontSizeY, _ := ui.fontSmall.SizeUTF8("A")
	level.Events.Do(func(s string, p int) {
		texture := ui.smallFontToTexture(s, sdl.Color{R: 255, G: 255, B: 255, A: 0})
		_, _, w, h, _ := texture.Query()
		ui.renderer.Copy(texture, nil, &sdl.Rect{X: 5, Y: int32(fontSizeY*p) + textStartY, W: w, H: h})
	})

	// ! Draw hp and mp on top
	ui.drawTopBar(level, barStart, barWidth)
}

// ! Top bar with HP and MP and Gold
func (ui *ui) drawTopBar(level *game.Level, X, W int32) {
	ui.renderer.Copy(ui.paperBarTexture, nil, &sdl.Rect{
		X: X,
		Y: 0,
		W: W,
		H: TILE_SIZE,
	})

	// ! HP
	topOffsetX := int32(float64(ui.winWidth) * .81)
	texture := ui.smallFontToTexture(
		fmt.Sprintf("HP: %d/%d", level.Player.Hitpoints, level.Player.MaxHitpoints),
		sdl.Color{R: 0, G: 0, B: 0, A: 0},
	)
	_, _, w, h, _ := texture.Query()
	ui.renderer.Copy(texture, nil, &sdl.Rect{X: topOffsetX, Y: 2, W: w, H: h})

	// ! MP
	topOffsetX = int32(float64(ui.winWidth) * .89)
	texture = ui.smallFontToTexture(
		fmt.Sprintf("MP: %d/%d", level.Player.ManaPoints, level.Player.MaxManaPoints),
		sdl.Color{R: 0, G: 0, B: 0, A: 0},
	)
	_, _, w, h, _ = texture.Query()
	ui.renderer.Copy(texture, nil, &sdl.Rect{X: topOffsetX, Y: 2, W: w, H: h})

	topOffsetX = int32(float64(ui.winWidth) * .96)
	texture = ui.smallFontToTexture(
		fmt.Sprintf("G: %d", level.Player.GetGold()),
		sdl.Color{R: 0, G: 0, B: 0, A: 0},
	)
	_, _, w, h, _ = texture.Query()
	ui.renderer.Copy(texture, nil, &sdl.Rect{X: topOffsetX, Y: 2, W: w, H: h})
}

// ! Draw hpbar
func (ui *ui) drawHpBarUnderCharacter(c *game.Character, offsetX, offsetY int32) {
	if c.HPBarTileID != 0 {
		hpbarSrc, _ := ui.texturesSrc[c.HPBarTileID]
		ui.renderer.Copy(ui.texturesAtlas, hpbarSrc, &sdl.Rect{
			X: int32(c.X*TILE_SIZE) + offsetX,
			Y: int32(c.Y*TILE_SIZE) + offsetY + 5,
			W: TILE_SIZE,
			H: TILE_SIZE})
	}
}

// ! Draw player
func (ui *ui) drawPlayer(level *game.Level, offsetX, offsetY int32, srcRect, dstRect *sdl.Rect) {
	if level.Player.Cloak != nil {
		srcCloak, _ := ui.texturesSrc[level.Player.Cloak.TileID]
		ui.renderer.Copy(ui.texturesAtlas, srcCloak, dstRect)
	}
	ui.renderer.Copy(ui.texturesAtlas, srcRect, dstRect)
	if level.Player.Head != nil {
		srcHead, _ := ui.texturesSrc[level.Player.Head.TileID]
		ui.renderer.Copy(ui.texturesAtlas, srcHead, dstRect)
	}
	if level.Player.Foot != nil {
		srcFoot, _ := ui.texturesSrc[level.Player.Foot.TileID]
		ui.renderer.Copy(ui.texturesAtlas, srcFoot, dstRect)
	}
	if level.Player.Body != nil {
		srcBody, _ := ui.texturesSrc[level.Player.Body.TileID]
		ui.renderer.Copy(ui.texturesAtlas, srcBody, dstRect)
	}
	if level.Player.Weapon != nil {
		srcWeapon, _ := ui.texturesSrc[level.Player.Weapon.TileID]
		ui.renderer.Copy(ui.texturesAtlas, srcWeapon, dstRect)
	}
}
