package ui

import (
	"fmt"

	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/NotDzhedai/rpg/game"
)

func (ui *ui) drawInventory(level *game.Level) {
	invRect := ui.getMainCenterRect()

	playerSrcRect := ui.texturesSrc[level.Player.TileID]
	ui.renderer.Copy(ui.inventoryBackground, nil, invRect)
	offset := int32(float64(invRect.H) * .1)
	dstRect := sdl.Rect{
		X: invRect.X + 30,
		Y: invRect.Y + offset,
		W: invRect.W / 2,
		H: invRect.H / 2,
	}

	ui.drawPlayer(level, 0, 0, playerSrcRect, &dstRect)

	// Slots
	// ! Head
	ui.renderer.Copy(ui.slotBackground, nil, ui.getHeadRect())
	if level.Player.Head != nil {
		ui.renderer.Copy(ui.texturesAtlas, ui.texturesSrc[level.Player.Head.TileID], ui.getHeadRect())
	}
	// ! Weapon
	ui.renderer.Copy(ui.slotBackground, nil, ui.getWeaponRect())
	if level.Player.Weapon != nil {
		ui.renderer.Copy(ui.texturesAtlas, ui.texturesSrc[level.Player.Weapon.TileID], ui.getWeaponRect())
	}
	// ! Body
	ui.renderer.Copy(ui.slotBackground, nil, ui.getBodyRect())
	if level.Player.Body != nil {
		ui.renderer.Copy(ui.texturesAtlas, ui.texturesSrc[level.Player.Body.TileID], ui.getBodyRect())
	}
	// ! Foot
	ui.renderer.Copy(ui.slotBackground, nil, ui.getFootRect())
	if level.Player.Foot != nil {
		ui.renderer.Copy(ui.texturesAtlas, ui.texturesSrc[level.Player.Foot.TileID], ui.getFootRect())
	}
	// ! Cloak
	ui.renderer.Copy(ui.slotBackground, nil, ui.getCloakRect())
	if level.Player.Cloak != nil {
		ui.renderer.Copy(ui.texturesAtlas, ui.texturesSrc[level.Player.Cloak.TileID], ui.getCloakRect())
	}

	// ! Stats
	ui.drawPlayerStats(level)

	// ! Inventory items
	for i, item := range level.Player.Inventory {
		itemSrcRect := ui.texturesSrc[item.TileID]
		if item == ui.draggedItem {
			ui.renderer.Copy(
				ui.texturesAtlas,
				itemSrcRect,
				&sdl.Rect{
					X: int32(ui.currentMouseState.pos.X),
					Y: int32(ui.currentMouseState.pos.Y),
					W: TILE_SIZE,
					H: TILE_SIZE,
				},
			)
		} else {
			ui.renderer.Copy(
				ui.texturesAtlas,
				itemSrcRect,
				ui.getInventoryItemRect(i),
			)
		}
	}

	ui.renderer.Copy(ui.borderTexture, nil, &sdl.Rect{
		X: invRect.X - 59,                  // magic number for make inv texture look good
		Y: invRect.Y - 110,                 // magic number for make inv texture look good
		W: int32(float64(invRect.W) * 1.2), // +20%
		H: int32(float64(invRect.H) * 1.4), // +40%
	})
}

func (ui *ui) drawPlayerStats(level *game.Level) {
	invRect := ui.getMainCenterRect()

	// HP, MP column
	// ! HP
	offsetX := int32(float64(invRect.W) * .6)
	offsetY := int32(float64(invRect.H) * .1)
	texture := ui.smallFontToTexture(
		fmt.Sprintf("HP: %d", level.Player.Hitpoints),
		sdl.Color{R: 0, G: 0, B: 0, A: 0},
	)
	_, _, w, h, _ := texture.Query()
	ui.renderer.Copy(
		texture,
		nil,
		&sdl.Rect{
			X: invRect.X + offsetX,
			Y: invRect.Y + offsetY,
			W: w,
			H: h,
		},
	)
	// ! MP
	offsetX = int32(float64(invRect.W) * .75)
	offsetY = int32(float64(invRect.H) * .1)
	texture = ui.smallFontToTexture(
		fmt.Sprintf("MP: %d", level.Player.ManaPoints),
		sdl.Color{R: 0, G: 0, B: 0, A: 0},
	)
	_, _, w, h, _ = texture.Query()
	ui.renderer.Copy(
		texture,
		nil,
		&sdl.Rect{
			X: invRect.X + offsetX,
			Y: invRect.Y + offsetY,
			W: w,
			H: h,
		},
	)

	// Damage, Armor, Intelligence column
	// ! row offset
	offsetX = int32(float64(invRect.W) * .6)
	offsetY = int32(float64(invRect.H) * .2)
	// ! Damage
	texture = ui.smallFontToTexture(
		fmt.Sprintf("Damage: %d", level.Player.CalcDamage()),
		sdl.Color{R: 0, G: 0, B: 0, A: 0},
	)
	_, _, w, h, _ = texture.Query()
	ui.renderer.Copy(
		texture,
		nil,
		&sdl.Rect{
			X: invRect.X + offsetX,
			Y: invRect.Y + offsetY,
			W: w,
			H: h,
		},
	)
	// ! Armor
	texture = ui.smallFontToTexture(
		fmt.Sprintf("Armor: %d", level.Player.CalcArmor()),
		sdl.Color{R: 0, G: 0, B: 0, A: 0},
	)
	_, _, w, h, _ = texture.Query()
	ui.renderer.Copy(
		texture,
		nil,
		&sdl.Rect{
			X: invRect.X + offsetX,
			Y: invRect.Y + offsetY + h,
			W: w,
			H: h,
		},
	)
	// ! Intelligence
	texture = ui.smallFontToTexture(
		fmt.Sprintf("Intelligence: %.2f%%", level.Player.CalcIntelligence()*100),
		sdl.Color{R: 0, G: 0, B: 0, A: 0},
	)
	_, _, w, h, _ = texture.Query()
	ui.renderer.Copy(
		texture,
		nil,
		&sdl.Rect{
			X: invRect.X + offsetX,
			Y: invRect.Y + offsetY + h*2,
			W: w,
			H: h,
		},
	)

}

func (ui *ui) getInventoryItemRect(index int) *sdl.Rect {
	invRect := ui.getMainCenterRect()
	offsetX := 20 // left from inventory texture
	offsetY := 60 // up from inventory texture
	return &sdl.Rect{
		X: invRect.X + int32(index)*TILE_SIZE + int32(offsetX),
		Y: invRect.Y + invRect.H - TILE_SIZE - int32(offsetY),
		W: TILE_SIZE,
		H: TILE_SIZE,
	}
}

func (ui *ui) getHeadRect() *sdl.Rect {
	invRect := ui.getMainCenterRect()
	offsetX := int32(float64(invRect.W) * .4)
	offsetY := int32(float64(invRect.H) * .08)
	return &sdl.Rect{
		X: invRect.X + offsetX,
		Y: invRect.Y + offsetY,
		W: TILE_SIZE,
		H: TILE_SIZE,
	}
}

func (ui *ui) getWeaponRect() *sdl.Rect {
	invRect := ui.getMainCenterRect()
	offsetY := int32(float64(invRect.H) * .35)
	offsetX := int32(float64(invRect.W) * .04)
	return &sdl.Rect{
		X: invRect.X + offsetX,
		Y: invRect.Y + offsetY,
		W: TILE_SIZE,
		H: TILE_SIZE,
	}
}

func (ui *ui) getBodyRect() *sdl.Rect {
	invRect := ui.getMainCenterRect()
	offsetY := int32(float64(invRect.H) * .2)
	offsetX := int32(float64(invRect.W) * .43)
	return &sdl.Rect{
		X: invRect.X + offsetX,
		Y: invRect.Y + offsetY,
		W: TILE_SIZE,
		H: TILE_SIZE,
	}
}

func (ui *ui) getFootRect() *sdl.Rect {
	invRect := ui.getMainCenterRect()
	offsetY := int32(float64(invRect.H) * .62)
	offsetX := int32(float64(invRect.W) * .27)
	return &sdl.Rect{
		X: invRect.X + offsetX,
		Y: invRect.Y + offsetY,
		W: TILE_SIZE,
		H: TILE_SIZE,
	}
}

func (ui *ui) getCloakRect() *sdl.Rect {
	invRect := ui.getMainCenterRect()
	offsetY := int32(float64(invRect.H) * .45)
	offsetX := int32(float64(invRect.W) * .48)
	return &sdl.Rect{
		X: invRect.X + offsetX,
		Y: invRect.Y + offsetY,
		W: TILE_SIZE,
		H: TILE_SIZE,
	}
}

func (ui *ui) getMainCenterRect() *sdl.Rect {
	invWidth := int32(float64(ui.winWidth) * .40)
	invHeight := int32(float64(ui.winHeigth) * .7)
	offsetX := (int32(ui.winWidth) - invWidth) / 2
	offsetY := (int32(ui.winHeigth) - invHeight) / 2
	return &sdl.Rect{
		X: offsetX,
		Y: offsetY,
		W: invWidth,
		H: invHeight,
	}
}

func (ui *ui) getItemPickupMenu(index int) *sdl.Rect {
	return &sdl.Rect{
		X: int32(ui.winWidth - TILE_SIZE - index*TILE_SIZE),
		Y: int32(ui.winHeigth) - TILE_SIZE,
		W: TILE_SIZE,
		H: TILE_SIZE,
	}
}

func (ui *ui) checkInventoryItems(level *game.Level) *game.Item {
	if ui.currentMouseState.leftButton {
		mousePos := ui.currentMouseState.pos
		for i, item := range level.Player.Inventory {
			itemRect := ui.getInventoryItemRect(i)
			if itemRect.HasIntersection(&sdl.Rect{X: int32(mousePos.X), Y: int32(mousePos.Y), W: 1, H: 1}) {
				return item
			}
		}
	}
	return nil
}

func (ui *ui) checkGroundItems(level *game.Level) *game.Item {
	if !ui.currentMouseState.leftButton && ui.prevMouseState.leftButton {
		items := level.Items[level.Player.Pos]
		mousePos := ui.currentMouseState.pos
		for i, item := range items {
			itemRect := ui.getItemPickupMenu(i)
			if itemRect.HasIntersection(&sdl.Rect{X: int32(mousePos.X), Y: int32(mousePos.Y), W: 1, H: 1}) {
				return item
			}
		}
	}
	return nil
}

func (ui *ui) checkDroppedItem() *game.Item {
	invRect := ui.getMainCenterRect()
	mousePos := ui.currentMouseState.pos
	if invRect.HasIntersection(&sdl.Rect{X: int32(mousePos.X), Y: int32(mousePos.Y), W: 1, H: 1}) {
		return nil
	}
	return ui.draggedItem
}

func (ui *ui) checkEquippedItem() *game.Item {
	mousePos := ui.currentMouseState.pos
	if ui.draggedItem.Type == game.Weapon {
		r := ui.getWeaponRect()
		if r.HasIntersection(&sdl.Rect{
			X: int32(mousePos.X),
			Y: int32(mousePos.Y),
			W: 1,
			H: 1,
		}) {
			return ui.draggedItem
		}
	} else if ui.draggedItem.Type == game.Head {
		r := ui.getHeadRect()
		if r.HasIntersection(&sdl.Rect{
			X: int32(mousePos.X),
			Y: int32(mousePos.Y),
			W: 1,
			H: 1,
		}) {
			return ui.draggedItem
		}
	} else if ui.draggedItem.Type == game.Body {
		r := ui.getBodyRect()
		if r.HasIntersection(&sdl.Rect{
			X: int32(mousePos.X),
			Y: int32(mousePos.Y),
			W: 1,
			H: 1,
		}) {
			return ui.draggedItem
		}
	} else if ui.draggedItem.Type == game.Cloak {
		r := ui.getCloakRect()
		if r.HasIntersection(&sdl.Rect{
			X: int32(mousePos.X),
			Y: int32(mousePos.Y),
			W: 1,
			H: 1,
		}) {
			return ui.draggedItem
		}
	} else if ui.draggedItem.Type == game.Foot {
		r := ui.getFootRect()
		if r.HasIntersection(&sdl.Rect{
			X: int32(mousePos.X),
			Y: int32(mousePos.Y),
			W: 1,
			H: 1,
		}) {
			return ui.draggedItem
		}
	}

	return nil
}
