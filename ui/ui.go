package ui

import (
	"math"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
	"gitlab.com/NotDzhedai/rpg/game"
)

const (
	TILE_SIZE               = 32
	SMALL_FONT_SIZE_PERCENT = .025
	POPUP_FONT_SIZE_PERCENT = .015
)

type ui struct {
	winWidth, winHeigth int

	state *stateMachine

	draggedItem *game.Item
	activeNpc   *game.NPC

	renderer *sdl.Renderer
	window   *sdl.Window

	texturesAtlas *sdl.Texture
	texturesSrc   map[int32]*sdl.Rect

	borderTexture   *sdl.Texture
	popupTexture    *sdl.Texture
	paperBarTexture *sdl.Texture

	inventoryBackground *sdl.Texture
	dialogBackground    *sdl.Texture
	slotBackground      *sdl.Texture

	keyboardState     []uint8
	prevKeyboardState []uint8

	centerX, centerY int

	levelChan chan *game.Level
	inputChan chan *game.Input

	fontSmall *ttf.Font
	fontPopup *ttf.Font

	str2TexSmall  map[string]*sdl.Texture
	str2TexDialog map[string]*sdl.Texture

	currentMouseState *mouseState
	prevMouseState    *mouseState
}

func init() {
	err := sdl.Init(sdl.INIT_EVERYTHING)
	if err != nil {
		panic(err)
	}

	err = img.Init(img.INIT_PNG)
	if err != nil {
		panic(err)
	}

	err = ttf.Init()
	if err != nil {
		panic(err)
	}

	// err = mix.Init(mix.INIT_OGG)
	// if err != nil {
	// 	panic(err)
	// }
}

func NewUI(inputChan chan *game.Input, levelChan chan *game.Level, width, height int) (*ui, error) {
	ui := &ui{}

	// ! State
	ui.state = createSM()

	// ! Init fields
	ui.winHeigth = height
	ui.winWidth = width
	ui.inputChan = inputChan
	ui.levelChan = levelChan

	ui.str2TexSmall = make(map[string]*sdl.Texture)
	ui.str2TexDialog = make(map[string]*sdl.Texture)
	ui.texturesSrc = make(map[int32]*sdl.Rect)

	// ! Init keyboard states
	ui.keyboardState = sdl.GetKeyboardState()
	ui.prevKeyboardState = make([]uint8, len(ui.keyboardState))
	for i, v := range ui.keyboardState {
		ui.prevKeyboardState[i] = v
	}

	// ! Create window
	var err error
	ui.window, err = sdl.CreateWindow(
		"RPG", 100, 100, int32(ui.winWidth), int32(ui.winHeigth), sdl.WINDOW_SHOWN,
	)
	if err != nil {
		return nil, err
	}

	// ! Create renderer
	ui.renderer, err = sdl.CreateRenderer(ui.window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		return nil, err
	}

	// ! Load texture atlas from png pic
	ui.texturesAtlas, err = img.LoadTexture(ui.renderer, "ui/assets/tiles.png")
	if err != nil {
		return nil, err
	}
	ui.texturesAtlas.SetBlendMode(sdl.BLENDMODE_BLEND)

	// ! load texture for inv menu
	ui.borderTexture, err = img.LoadTexture(ui.renderer, "ui/assets/border.png")
	if err != nil {
		return nil, err
	}
	ui.borderTexture.SetBlendMode(sdl.BLENDMODE_BLEND)

	// ! load texture for npc popup
	ui.popupTexture, err = img.LoadTexture(ui.renderer, "ui/assets/dialog.png")
	if err != nil {
		return nil, err
	}
	ui.popupTexture.SetAlphaMod(180)
	ui.popupTexture.SetBlendMode(sdl.BLENDMODE_BLEND)

	ui.paperBarTexture, err = img.LoadTexture(ui.renderer, "ui/assets/paper_bar.png")
	if err != nil {
		return nil, err
	}
	ui.paperBarTexture.SetAlphaMod(180)
	ui.paperBarTexture.SetBlendMode(sdl.BLENDMODE_BLEND)

	ui.dialogBackground, err = img.LoadTexture(ui.renderer, "ui/assets/paper.png")
	ui.dialogBackground.SetBlendMode(sdl.BLENDMODE_BLEND)

	// ! temp. For camera
	ui.centerX = -1
	ui.centerY = -1

	// ! Load critial tiles
	ui.saveCriticalDataToTextureSrc([]int32{
		game.Staff1,
		game.Staff2,
		game.Staff3,
		game.AdeptStaff1,
		game.AdeptStaff2,
		game.AdeptStaff3,
		game.Hat1,
		game.Hat2,
		game.Hat3,
		game.AdeptHat1,
		game.AdeptHat2,
		game.AdeptHat3,
		game.Body1,
		game.Body2,
		game.Body3,
		game.AdeptBody1,
		game.AdeptBody2,
		game.AdeptBody3,
		game.Cloak1,
		game.Cloak2,
		game.Cloak3,
		game.AdeptCloak1,
		game.AdeptCloak2,
		game.AdeptCloak3,
		game.Foot1,
		game.Foot2,
		game.Foot3,
		game.AdeptFoot1,
		game.AdeptFoot2,
		game.AdeptFoot3,
		game.HPBar10,
		game.HPBar20,
		game.HPBar40,
		game.HPBar60,
		game.HPBar80,
	})

	// ! Init fonts
	ui.fontSmall, err = ttf.OpenFont(
		"ui/assets/gothic.ttf",
		int(float64(ui.winHeigth)*SMALL_FONT_SIZE_PERCENT),
	)
	if err != nil {
		return nil, err
	}
	ui.fontPopup, err = ttf.OpenFont(
		"ui/assets/gothic.ttf",
		int(float64(ui.winHeigth)*POPUP_FONT_SIZE_PERCENT),
	)
	if err != nil {
		return nil, err
	}

	// ! Init background textures
	ui.inventoryBackground = ui.getSinglePixelTex(sdl.Color{R: 209, G: 170, B: 63, A: 200})
	ui.inventoryBackground.SetBlendMode(sdl.BLENDMODE_BLEND)

	ui.slotBackground = ui.getSinglePixelTex(sdl.Color{R: 66, G: 50, B: 7, A: 0})

	return ui, nil
}

// ! Create tex
func (ui *ui) getSinglePixelTex(color sdl.Color) *sdl.Texture {
	texture, err := ui.renderer.CreateTexture(sdl.PIXELFORMAT_ABGR8888, sdl.TEXTUREACCESS_STATIC, 1, 1)
	if err != nil {
		panic(err)
	}
	pixels := make([]byte, 4)
	pixels[0] = color.R
	pixels[1] = color.G
	pixels[2] = color.B
	pixels[3] = color.A
	texture.Update(nil, pixels, 4)
	return texture
}

// ! load to map criitical data
func (ui *ui) saveCriticalDataToTextureSrc(arr []int32) {
	for _, id := range arr {
		py := int32(math.Floor(float64(id / 64)))
		px := id - (py * 64)
		ui.texturesSrc[id] = &sdl.Rect{X: px * TILE_SIZE, Y: py * TILE_SIZE, W: TILE_SIZE, H: TILE_SIZE}
	}
}

func (ui *ui) keyDownOnce(key uint8) bool {
	return ui.keyboardState[key] == 1 && ui.prevKeyboardState[key] == 0
}

func (ui *ui) keyPressed(key uint8) bool {
	return ui.keyboardState[key] == 1 && ui.prevKeyboardState[key] == 1
}

// ! main loop
func (ui *ui) Run() {
	var newLevel *game.Level
	ui.prevMouseState = getMouseState()

	for {
		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
			switch event.(type) {
			case *sdl.QuitEvent:
				ui.inputChan <- &game.Input{Type: game.QuitGame}
				return
			}
		}

		ui.currentMouseState = getMouseState()

		var ok bool
		select {
		case newLevel, ok = <-ui.levelChan:
			// TODO handle sounds
			if ok {
			}
		default:
		}

		// ! Init input to sent in game
		input := &game.Input{}

		// ! Execute state
		ui.state.update(ui, input, newLevel)

		// ! present data on screen
		ui.renderer.Present()

		// ! take single item from ground
		item := ui.checkGroundItems(newLevel)
		if item != nil {
			input.Type = game.TakeItem
			input.Item = item
		}

		// ! handle keyboard events
		if sdl.GetKeyboardFocus() == ui.window || sdl.GetMouseFocus() == ui.window {
			if ui.keyDownOnce(sdl.SCANCODE_W) {
				input.Type = game.Up
				// sdl.Delay(100)
			} else if ui.keyDownOnce(sdl.SCANCODE_S) {
				input.Type = game.Down
				// sdl.Delay(100)
			} else if ui.keyDownOnce(sdl.SCANCODE_D) {
				input.Type = game.Right
				// sdl.Delay(100)
			} else if ui.keyDownOnce(sdl.SCANCODE_A) {
				input.Type = game.Left
				// sdl.Delay(100)
			} else if ui.keyDownOnce(sdl.SCANCODE_T) {
				input.Type = game.TakeAllItems
			} else if ui.keyDownOnce(sdl.SCANCODE_I) {
				ui.state.change(uiInventory, ui)
			} else if ui.keyDownOnce(sdl.SCANCODE_ESCAPE) {
				ui.state.change(uiMain, ui)
			}

			// ! update prev keyboard state
			for i, v := range ui.keyboardState {
				ui.prevKeyboardState[i] = v
			}

			if input.Type != game.None {
				ui.inputChan <- input
			}
		}

		// ! update prev mouse state
		ui.prevMouseState = ui.currentMouseState

		sdl.Delay(50)
	}
}
