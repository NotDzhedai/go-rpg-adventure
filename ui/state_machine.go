package ui

import (
	"gitlab.com/NotDzhedai/rpg/game"
)

type stateType int

const (
	uiMain stateType = iota
	uiInventory
	uiDialog
	uiTrade
)

type state interface {
	enter(ui *ui, params ...interface{})
	change(state stateType, ui *ui, params ...interface{})
	update(ui *ui, input *game.Input, level *game.Level)
	exit(ui *ui)
}

type stateMachine struct {
	states  map[stateType]state
	Current state
}

func createSM() *stateMachine {
	ms := newMainState()
	return &stateMachine{
		states: map[stateType]state{
			uiMain:      ms,
			uiInventory: newInventoryState(),
			uiDialog:    newDialogState(),
			uiTrade:     newTradeState(),
		},
		Current: ms,
	}
}

func (sm *stateMachine) enter(ui *ui, params ...interface{}) {
	sm.Current.enter(ui, params...)
}

func (sm *stateMachine) update(ui *ui, input *game.Input, level *game.Level) {
	sm.Current.update(ui, input, level)
}

func (sm *stateMachine) exit(ui *ui) {
	sm.Current.exit(ui)
}

func (sm *stateMachine) change(state stateType, ui *ui, params ...interface{}) {
	if sm.Current != nil {
		sm.Current.exit(ui)
	}
	sm.Current = sm.states[state]
	sm.Current.enter(ui, params...)
}
