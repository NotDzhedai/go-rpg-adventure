package ui

import (
	"github.com/veandco/go-sdl2/sdl"
)

type FontSize int

func (ui *ui) smallFontToTexture(s string, color sdl.Color) *sdl.Texture {
	texture, exists := ui.str2TexSmall[s]
	if exists {
		return texture
	}

	fontSurface, err := ui.fontSmall.RenderUTF8BlendedWrapped(s, color, 400)
	if err != nil {
		panic(err)
	}

	texture, err = ui.renderer.CreateTextureFromSurface(fontSurface)
	if err != nil {
		panic(err)
	}

	ui.str2TexSmall[s] = texture

	return texture
}

func (ui *ui) dialogFontToTexture(s string, color sdl.Color) *sdl.Texture {
	texture, exists := ui.str2TexDialog[s]
	if exists {
		return texture
	}

	fontSurface, err := ui.fontPopup.RenderUTF8BlendedWrapped(s, color, 70) // max widt for text befor wrap
	if err != nil {
		panic(err)
	}

	texture, err = ui.renderer.CreateTextureFromSurface(fontSurface)
	if err != nil {
		panic(err)
	}

	ui.str2TexDialog[s] = texture

	return texture
}
