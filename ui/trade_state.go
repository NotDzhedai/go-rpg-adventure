package ui

import (
	"gitlab.com/NotDzhedai/rpg/game"
)

type tradeState struct{}

func newTradeState() state {
	return &tradeState{}
}

// change implements state
func (*tradeState) change(state stateType, ui *ui, params ...interface{}) {}

// enter implements state
func (*tradeState) enter(ui *ui, params ...interface{}) {
	npc, ok := params[0].(*game.NPC)
	if !ok {
		panic("NPC error convert")
	}

	ui.activeNpc = npc
}

// exit implements state
func (*tradeState) exit(ui *ui) {
	ui.activeNpc.CloseTrade()
	ui.activeNpc = nil
}

// update implements state
func (*tradeState) update(ui *ui, input *game.Input, level *game.Level) {
	ui.renderer.Clear()
	ui.drawPlayerInventory(input, level)
	ui.drawTraderInventory(input, level)

	playerSellItem := ui.checkPlayerItem(level)
	if playerSellItem != nil {
		input.Type = game.PlayerSellItem
		input.Item = playerSellItem
		input.Npc = ui.activeNpc
	}
	playerBuyItem := ui.checkTraderItem(level)
	if playerBuyItem != nil {
		input.Type = game.PlayerBuyItem
		input.Item = playerBuyItem
		input.Npc = ui.activeNpc
	}

	if !ui.activeNpc.InRange(level.Player.Pos) {
		ui.state.change(uiMain, ui)
	}
}
