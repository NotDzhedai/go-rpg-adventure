package main

import (
	"flag"

	"gitlab.com/NotDzhedai/rpg/game"
	"gitlab.com/NotDzhedai/rpg/ui"
)

func main() {
	startLvl := flag.String("lvl", "1", "select start level")
	flag.Parse()

	game, err := game.NewGame(*startLvl)
	if err != nil {
		panic(err)
	}

	go game.Run()
	ui, err := ui.NewUI(game.InputChan, game.LevelChan, 1400, 800)
	if err != nil {
		panic(err)
	}
	ui.Run()
}
