package game

import (
	"math/rand"
	"time"
)

var allItems = []int32{
	// Weapon
	Staff1, Staff2, Staff3, AdeptStaff1, AdeptStaff2, AdeptStaff3, Trident,
	// Hat
	Hat1, Hat2, Hat3, AdeptHat1, AdeptHat2, AdeptHat3,
	// Foot
	Foot1, Foot2, Foot3, AdeptFoot1, AdeptFoot2, AdeptFoot3,
	// Cloak
	Cloak1, Cloak2, Cloak3, AdeptCloak1, AdeptCloak2, AdeptCloak3,
	// Body
	Body1, Body2, Body3, AdeptBody1, AdeptBody2, AdeptBody3,
}

type itemType int

const (
	Head itemType = iota
	Weapon
	Body
	Cloak
	Foot
)

var itemsLVL1 = []int{
	Hat1, Hat2, Hat3,
	Body1, Body2, Body3,
	Cloak1, Cloak2, Cloak3,
	Foot1, Foot2, Foot3,
	Staff1, Staff2, Staff3,
}

type Item struct {
	entity
	Characteristics
	Type    itemType
	HPBonus int
	MPBonus int
	Lvl     int
	Price   int
}

func randomItemLVL1(pos Pos) *Item {
	rand.Seed(time.Now().UTC().UnixNano())
	ri := rand.Intn(len(itemsLVL1))
	res := newItem(pos, int32(itemsLVL1[ri]))
	return res
}

// ? rework
func newItem(pos Pos, tileID int32) *Item {
	item := &Item{}
	item.Pos = pos
	item.TileID = tileID

	switch tileID {
	case Staff1:
		item.Name = "Staff 1"
		item.Lvl = 1
		item.Type = Weapon
		item.HPBonus = 0
		item.MPBonus = 10
		item.Characteristics = Characteristics{
			Damage:       2,
			Armor:        0,
			Intelligence: .04,
		}
		item.Price = 5
	case Staff2:
		item.Name = "Staff 2"
		item.Lvl = 1
		item.Type = Weapon
		item.HPBonus = 0
		item.MPBonus = 10
		item.Characteristics = Characteristics{
			Damage:       1,
			Armor:        0,
			Intelligence: .04,
		}
		item.Price = 5
	case Staff3:
		item.Name = "Staff 3"
		item.Lvl = 1
		item.Type = Weapon
		item.HPBonus = 0
		item.MPBonus = 10
		item.Characteristics = Characteristics{
			Damage:       3,
			Armor:        0,
			Intelligence: .03,
		}
		item.Price = 5
	case AdeptStaff1:
		item.Name = "AdeptStaff1"
		item.Lvl = 2
		item.Type = Weapon
		item.HPBonus = 0
		item.MPBonus = 15
		item.Characteristics = Characteristics{
			Damage:       4,
			Armor:        0,
			Intelligence: .08,
		}
		item.Price = 10
	case AdeptStaff2:
		item.Name = "AdeptStaff2"
		item.Lvl = 2
		item.Type = Weapon
		item.HPBonus = 0
		item.MPBonus = 15
		item.Characteristics = Characteristics{
			Damage:       1,
			Armor:        0,
			Intelligence: .08,
		}
		item.Price = 10
	case AdeptStaff3:
		item.Name = "AdeptStaff3"
		item.Lvl = 2
		item.Type = Weapon
		item.HPBonus = 0
		item.MPBonus = 15
		item.Characteristics = Characteristics{
			Damage:       3,
			Armor:        0,
			Intelligence: .07,
		}
		item.Price = 10
	case Trident:
		item.Name = "Trident"
		item.Lvl = 2
		item.Type = Weapon
		item.HPBonus = 50
		item.MPBonus = 10
		item.Characteristics = Characteristics{
			Damage:       20,
			Armor:        0,
			Intelligence: 0,
		}
		item.Price = 50
	case Hat1:
		item.Name = "Hat 1"
		item.Lvl = 1
		item.Type = Head
		item.HPBonus = 5
		item.MPBonus = 0
		item.Characteristics = Characteristics{
			Damage:       0,
			Armor:        2,
			Intelligence: .02,
		}
		item.Price = 5
	case Hat2:
		item.Name = "Hat 2"
		item.Lvl = 1
		item.Type = Head
		item.HPBonus = 5
		item.MPBonus = 0
		item.Characteristics = Characteristics{
			Damage:       1,
			Armor:        1,
			Intelligence: .02,
		}
		item.Price = 5
	case Hat3:
		item.Name = "Hat 3"
		item.Lvl = 1
		item.Type = Head
		item.HPBonus = 5
		item.MPBonus = 0
		item.Characteristics = Characteristics{
			Damage:       0,
			Armor:        1,
			Intelligence: .03,
		}
		item.Price = 5
	case AdeptHat1:
		item.Name = "AdeptHat1"
		item.Lvl = 2
		item.Type = Head
		item.HPBonus = 5
		item.MPBonus = 5
		item.Characteristics = Characteristics{
			Damage:       0,
			Armor:        2,
			Intelligence: .04,
		}
		item.Price = 10
	case AdeptHat2:
		item.Name = "AdeptHat2"
		item.Lvl = 2
		item.Type = Head
		item.HPBonus = 5
		item.MPBonus = 5
		item.Characteristics = Characteristics{
			Damage:       2,
			Armor:        1,
			Intelligence: .03,
		}
		item.Price = 10
	case AdeptHat3:
		item.Name = "AdeptHat3"
		item.Lvl = 2
		item.Type = Head
		item.HPBonus = 5
		item.MPBonus = 5
		item.Characteristics = Characteristics{
			Damage:       1,
			Armor:        1,
			Intelligence: .04,
		}
		item.Price = 10
	case Cloak1:
		item.Name = "Cloak1"
		item.Lvl = 1
		item.Type = Cloak
		item.HPBonus = 5
		item.MPBonus = 0
		item.Characteristics = Characteristics{
			Damage:       0,
			Armor:        2,
			Intelligence: 0,
		}
		item.Price = 5
	case Cloak2:
		item.Name = "Cloak2"
		item.Lvl = 1
		item.Type = Cloak
		item.HPBonus = 5
		item.MPBonus = 0
		item.Characteristics = Characteristics{
			Damage:       0,
			Armor:        1,
			Intelligence: .01,
		}
		item.Price = 5
	case Cloak3:
		item.Name = "Cloak3"
		item.Lvl = 1
		item.Type = Cloak
		item.HPBonus = 5
		item.MPBonus = 0
		item.Characteristics = Characteristics{
			Damage:       1,
			Armor:        1,
			Intelligence: 0,
		}
		item.Price = 5
	case AdeptCloak1:
		item.Name = "AdeptCloak1"
		item.Lvl = 2
		item.Type = Cloak
		item.HPBonus = 10
		item.MPBonus = 0
		item.Characteristics = Characteristics{
			Damage:       0,
			Armor:        4,
			Intelligence: 0,
		}
		item.Price = 10
	case AdeptCloak2:
		item.Name = "AdeptCloak2"
		item.Lvl = 2
		item.Type = Cloak
		item.HPBonus = 10
		item.MPBonus = 0
		item.Characteristics = Characteristics{
			Damage:       1,
			Armor:        3,
			Intelligence: 0,
		}
		item.Price = 10
	case AdeptCloak3:
		item.Name = "AdeptCloak3"
		item.Lvl = 2
		item.Type = Cloak
		item.HPBonus = 10
		item.MPBonus = 0
		item.Characteristics = Characteristics{
			Damage:       0,
			Armor:        3,
			Intelligence: .01,
		}
		item.Price = 10
	case Body1:
		item.Name = "Body1"
		item.Lvl = 1
		item.Type = Body
		item.HPBonus = 5
		item.MPBonus = 5
		item.Characteristics = Characteristics{
			Damage:       2,
			Armor:        2,
			Intelligence: 0,
		}
		item.Price = 5
	case Body2:
		item.Name = "Body2"
		item.Lvl = 1
		item.Type = Body
		item.HPBonus = 5
		item.MPBonus = 5
		item.Characteristics = Characteristics{
			Damage:       1,
			Armor:        3,
			Intelligence: 0,
		}
		item.Price = 5
	case Body3:
		item.Name = "Body3"
		item.Lvl = 1
		item.Type = Body
		item.HPBonus = 5
		item.MPBonus = 5
		item.Characteristics = Characteristics{
			Damage:       0,
			Armor:        3,
			Intelligence: .01,
		}
		item.Price = 5
	case AdeptBody1:
		item.Name = "AdeptBody1"
		item.Lvl = 2
		item.Type = Body
		item.HPBonus = 5
		item.MPBonus = 10
		item.Characteristics = Characteristics{
			Damage:       0,
			Armor:        3,
			Intelligence: .03,
		}
		item.Price = 10
	case AdeptBody2:
		item.Name = "AdeptBody2"
		item.Lvl = 2
		item.Type = Body
		item.HPBonus = 5
		item.MPBonus = 10
		item.Characteristics = Characteristics{
			Damage:       0,
			Armor:        2,
			Intelligence: .04,
		}
		item.Price = 10
	case AdeptBody3:
		item.Name = "AdeptBody3"
		item.Lvl = 2
		item.Type = Body
		item.HPBonus = 5
		item.MPBonus = 10
		item.Characteristics = Characteristics{
			Damage:       2,
			Armor:        2,
			Intelligence: .02,
		}
		item.Price = 10
	case Foot1:
		item.Name = "Foot1"
		item.Lvl = 1
		item.Type = Foot
		item.HPBonus = 5
		item.MPBonus = 0
		item.Characteristics = Characteristics{
			Damage:       2,
			Armor:        2,
			Intelligence: 0,
		}
		item.Price = 5
	case Foot2:
		item.Name = "Foot2"
		item.Lvl = 1
		item.Type = Foot
		item.HPBonus = 5
		item.MPBonus = 0
		item.Characteristics = Characteristics{
			Damage:       1,
			Armor:        2,
			Intelligence: .01,
		}
		item.Price = 5
	case Foot3:
		item.Name = "Foot3"
		item.Lvl = 1
		item.Type = Foot
		item.HPBonus = 5
		item.MPBonus = 0
		item.Characteristics = Characteristics{
			Damage:       1,
			Armor:        1,
			Intelligence: .02,
		}
		item.Price = 5
	case AdeptFoot1:
		item.Name = "AdeptFoot1"
		item.Lvl = 2
		item.Type = Foot
		item.HPBonus = 10
		item.MPBonus = 0
		item.Characteristics = Characteristics{
			Damage:       0,
			Armor:        4,
			Intelligence: .02,
		}
		item.Price = 10
	case AdeptFoot2:
		item.Name = "AdeptFoot2"
		item.Lvl = 2
		item.Type = Foot
		item.HPBonus = 10
		item.MPBonus = 0
		item.Characteristics = Characteristics{
			Damage:       1,
			Armor:        3,
			Intelligence: .02,
		}
		item.Price = 10
	case AdeptFoot3:
		item.Name = "AdeptFoot3"
		item.Lvl = 2
		item.Type = Foot
		item.HPBonus = 10
		item.MPBonus = 0
		item.Characteristics = Characteristics{
			Damage:       0,
			Armor:        3,
			Intelligence: .03,
		}
		item.Price = 10
	}

	return item
}
