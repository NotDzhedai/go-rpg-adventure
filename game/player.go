package game

type Player struct {
	Character
	LastCheckPoint Pos
}

func newPlayer() *Player {
	player := &Player{}
	player.TileID = Hero

	player.Name = "Hero"
	player.Flip = false
	player.Gold = 25

	player.InventoryMax = 14
	player.Inventory = []*Item{}

	player.Damage = 50
	player.Armor = 0
	player.Intelligence = .05

	player.Hitpoints = 50
	player.MaxHitpoints = 50
	player.ManaPoints = 10
	player.MaxManaPoints = 10

	player.Speed = 1.0
	player.ActioinPoints = 0
	player.SightRange = 6

	return player
}

func (p *Player) setNewCheckpoint(pos Pos) {
	p.LastCheckPoint = pos
}

func (p *Player) healHPInShrine() {
	p.Hitpoints = p.MaxHitpoints
}

func (p *Player) healMPInShrine() {
	p.ManaPoints = p.MaxManaPoints
}

func (p *Player) buyItem(item *Item) {
	p.Gold -= item.Price
	p.Inventory = append(p.Inventory, item)
}

func (p *Player) sellItem(sellItem *Item) {
	p.Gold += sellItem.Price
	for i, item := range p.Inventory {
		if item == sellItem {
			p.Inventory = append(p.Inventory[:i], p.Inventory[i+1:]...)
			return
		}
	}
}
