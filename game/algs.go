package game

import (
	"math"
)

func (level *Level) astar(start Pos, goal Pos) []Pos {
	frontier := make(pqueue, 0, 8)
	frontier = frontier.push(start, 1)
	cameFrom := make(map[Pos]Pos)
	cameFrom[start] = start
	costSoFar := make(map[Pos]int)
	costSoFar[start] = 0

	var current Pos
	for len(frontier) > 0 {
		frontier, current = frontier.pop()

		if current == goal {
			path := make([]Pos, 0)
			p := current
			for p != start {
				path = append(path, p)
				p = cameFrom[p]
			}
			path = append(path, p)
			for i, j := 0, len(path)-1; i < j; i, j = i+1, j-1 {
				path[i], path[j] = path[j], path[i]
			}

			return path
		}

		for _, next := range level.getNeighbors(current, level.canWalk) {
			newCost := costSoFar[current] + 1
			_, exist := costSoFar[next]
			if !exist || newCost < costSoFar[next] {
				costSoFar[next] = newCost
				xDist := int(math.Abs(float64(goal.X - next.X)))
				yDist := int(math.Abs(float64(goal.Y - next.Y)))
				priority := newCost + xDist + yDist
				frontier = frontier.push(next, priority)
				cameFrom[next] = current
			}
		}
	}

	return nil
}

func (level *Level) getNeighbors(p Pos, fn func(pos Pos) bool) []Pos {
	neighbors := make([]Pos, 0, 4)
	left := Pos{p.X - 1, p.Y}
	right := Pos{p.X + 1, p.Y}
	up := Pos{p.X, p.Y - 1}
	down := Pos{p.X, p.Y + 1}

	if fn(right) {
		neighbors = append(neighbors, right)
	}
	if fn(left) {
		neighbors = append(neighbors, left)
	}
	if fn(up) {
		neighbors = append(neighbors, up)
	}
	if fn(down) {
		neighbors = append(neighbors, down)
	}

	return neighbors
}

func (level *Level) bresenham(start, end Pos) {
	steep := math.Abs(float64(end.Y-start.Y)) > math.Abs(float64(end.X-start.X))
	if steep {
		start.X, start.Y = start.Y, start.X
		end.X, end.Y = end.Y, end.X
	}

	deltaY := int(math.Abs(float64(end.Y - start.Y)))
	err := 0
	y := start.Y
	ystep := 1
	if start.Y >= end.Y {
		ystep = -1
	}

	if start.X > end.X {
		deltaX := start.X - end.X
		for x := start.X; x > end.X; x-- {
			var pos Pos
			if steep {
				pos = Pos{y, x}
			} else {
				pos = Pos{x, y}
			}
			if pos.X < 60 && pos.X >= 0 && pos.Y < 25 && pos.Y >= 0 {
				level.Map[pos.Y][pos.X].Visible = true
				level.Map[pos.Y][pos.X].Seen = true
			}
			if !level.canSeeThrough(pos) {
				return
			}
			err += deltaY
			if 2*err >= deltaX {
				y += ystep
				err -= deltaX
			}
		}
	} else {
		deltaX := end.X - start.X
		for x := start.X; x < end.X; x++ {
			var pos Pos
			if steep {
				pos = Pos{y, x}
			} else {
				pos = Pos{x, y}
			}
			if pos.X < 60 && pos.X >= 0 && pos.Y < 25 && pos.Y >= 0 {
				level.Map[pos.Y][pos.X].Visible = true
				level.Map[pos.Y][pos.X].Seen = true
			}
			if !level.canSeeThrough(pos) {
				return
			}
			err += deltaY
			if 2*err >= deltaX {
				y += ystep
				err -= deltaX
			}
		}
	}
}
