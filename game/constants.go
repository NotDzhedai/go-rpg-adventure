package game

const (
	// decorations
	ClosedDoor       = 100
	OpenedDoor       = 115
	SecretDoor       = 914
	LockedDoorSingle = 116
	LockedChest      = 6
	ClosedChest      = 7
	OpenedChest      = 8
	MPShrine         = 3
	HPShrine         = 1
	Flag             = 1357
	StoneGangoyle    = 787
	BunchOfSkulls    = 45

	TorchOff = 1358
	TorchOn  = 1359
	BigFire  = 53

	GravesStart, GravesEnd         = 56, 61
	TreesStart, TreesEnd           = 845, 853
	SculpturesStart, SculpturesEnd = 781, 786

	// Walls
	BrickWallsStart, BrickWallsEnd = 265, 278
	StoneWallsStart, StoneWallsEnd = 910, 925
	DirtWallsStart, DirtWallsEnd   = 448, 458
	RuneWallsStart, RuneWallsEnd   = 1153, 1161
	LockedDoorStart, LockedDoorEnd = 109, 111

	// Water
	WaterStart, WaterEnd = 1473, 1477

	// Danger zones
	Lava = 384

	// Gold
	GoldStart, GoldEnd = 2601, 2614
	Gold1              = 2601
	Gold2              = 2605
	Gold3              = 2608
	Gold4              = 2609
	Gold5              = 2610
	Gold6              = 2611
	Gold7              = 2612
	Gold8              = 2613
	Gold9              = 2614

	// Portals
	PortalsStart, PortalsEnd = 674, 769
	DungeonPortal            = 744
	MagicPortal              = 766
	CavePortal               = 763
	MagicGreenPortal         = 690
	ShopPortal               = 769
	BrokenPortal             = 767

	// Player
	Hero = 5088

	// HPBar
	HPBar80 = 3196
	HPBar60 = 3197
	HPBar40 = 3195
	HPBar20 = 3198
	HPBar10 = 3194

	// Monsters
	Skeleton            = 4799
	SkeletonWithSpit    = 4228
	Bat                 = 4065
	Frog                = 4072
	Spider              = 4125
	SkeletonNecromancer = 4733
	Rat                 = 4124
	Ogre                = 3784
	FireElemental       = 4385
	RedDragon           = 6024
	IceDragon           = 6025
	IceMonster          = 5058
	WormHead            = 4859
	RedLizardWarrior    = 4290
	LavaWorm            = 4169

	// NPCs
	TownGuardian   = 3802
	MagicTrader    = 3807
	ArmorTrader    = 3808
	GhnomeTrader   = 3790
	Pilgrim        = 3869
	Archmage       = 3800
	GnomeMage      = 3840
	DarkMage       = 3796
	GhnomeGuard1   = 3815
	GhnomeGuard2   = 3816
	GhnomeCustomer = 3789

	// Items
	// ! Weapons
	// lvl1
	Staff1 = 5706
	Staff2 = 5816
	Staff3 = 5812
	// lvl2
	AdeptStaff1 = 5811
	AdeptStaff2 = 5815
	AdeptStaff3 = 5766
	Trident     = 5787
	// ! Head
	// lvl1
	Hat1 = 5974
	Hat2 = 5967
	Hat3 = 5903
	// lvl2
	AdeptHat1 = 5965
	AdeptHat2 = 5966
	AdeptHat3 = 5973
	// ! Body
	// lvl1
	Body1 = 5317
	Body2 = 5334
	Body3 = 5323
	// lvl2
	AdeptBody1 = 5313
	AdeptBody2 = 5327
	AdeptBody3 = 5330
	// ! Cloak
	// lvl1
	Cloak1 = 5383
	Cloak2 = 5384
	Cloak3 = 5385
	// lvl2
	AdeptCloak1 = 5392
	AdeptCloak2 = 5393
	AdeptCloak3 = 5394
	// ! Foot
	// lvl1
	Foot1 = 5369
	Foot2 = 5370
	Foot3 = 5371
	// lvl2
	AdeptFoot1 = 5373
	AdeptFoot2 = 5374
	AdeptFoot3 = 5375
)
