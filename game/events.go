package game

type EventsArray struct {
	Events       []string
	EventPositon int
	Len          int
}

func newEventsArray(l int) *EventsArray {
	ea := make([]string, l)
	for i := range ea {
		ea[i] = " "
	}
	return &EventsArray{
		Len:          l,
		EventPositon: 0,
		Events:       ea,
	}
}

func (ea *EventsArray) addEvent(event string) {
	ea.Events[ea.EventPositon] = event
	ea.EventPositon++
	if ea.EventPositon == ea.Len {
		ea.EventPositon = 0
	}
}

func (ea *EventsArray) Do(fn func(string, int)) {
	i := ea.EventPositon
	count := 0
	for {
		event := ea.Events[i]
		fn(event, count)
		i = (i + 1) % ea.Len
		count++
		if i == ea.EventPositon {
			break
		}
	}
}
