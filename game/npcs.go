package game

import (
	"math/rand"
	"strings"
	"time"
)

type NPCStatus int

const (
	Active NPCStatus = iota
	NotActive
)

type NPCType int

const (
	Trader NPCType = iota
	Guard
	Regular
)

type Choice struct {
	Option   string
	NextNode *StoryNode
}

type StoryNode struct {
	Text    string
	Choices []*Choice
}

func (node *StoryNode) addChoice(option string, nextNode *StoryNode) {
	choice := &Choice{option, nextNode}
	node.Choices = append(node.Choices, choice)
}

const trade = "[Trade]"

type NPC struct {
	Character
	InteractZones   []Pos
	Status          NPCStatus
	Messages        []string
	CurrentMessage  string
	Type            NPCType
	IsDialogStarted bool
	IsTradeStarted  bool
	ItemToSell      []*Item
	*StoryNode
}

func newNPC(p Pos, name string, tileID int32) *NPC {
	npc := &NPC{}

	npc.Name = name
	npc.TileID = tileID
	npc.Pos = p
	npc.Status = NotActive
	npc.Type = Regular
	npc.Lvl = 1
	npc.InventoryMax = 14

	npc.Hitpoints = 100
	npc.Damage = 0
	npc.Speed = 0
	npc.ActioinPoints = 0.0
	npc.SightRange = 0

	npc.IsTradeStarted = false

	npc.setInteractZones()

	npc.Messages = []string{
		"Hello",
		"How are you?",
		"You look tired",
	}

	firstNode := StoryNode{Text: "Hello traveler! What are you doing here?"}
	secondNode := StoryNode{Text: "Ah, adventure. I used to be like that too ..."}
	thirdNode := StoryNode{Text: "Then you have chosen not the best place to look for money"}

	firstNode.addChoice("Looking for adventure", &secondNode)
	firstNode.addChoice("I'm trying to make a living", &thirdNode)
	firstNode.addChoice("I need money...", &thirdNode)

	secondNode.addChoice("Back", &firstNode)
	thirdNode.addChoice("Back", &firstNode)

	npc.StoryNode = &firstNode

	return npc
}

func newTrader(p Pos, name string, tileID int32) *NPC {
	t := newNPC(p, name, tileID)
	t.Gold = 100

	rand.Seed(time.Now().UTC().UnixNano())
	for i := 0; i < rand.Intn(10)+1; i++ {
		item := randomItemLVL1(t.Pos)
		t.ItemToSell = append(t.ItemToSell, item)
	}

	t.Messages = []string{
		"Look at my products",
		"Discounts just for you",
		"Have time to buy what you need",
	}

	firstNode := StoryNode{Text: "I sincerely congratulate you. What I can do for you?"}
	secondNode := StoryNode{Text: "Of course, I have only the best things"}

	firstNode.addChoice("Let me look at your products", &secondNode)
	secondNode.addChoice(trade, &firstNode)
	secondNode.addChoice("Back", &firstNode)

	t.StoryNode = &firstNode

	return t
}

func newGuard(p Pos, name string, tileID int32) *NPC {
	g := newNPC(p, name, tileID)
	g.Messages = []string{
		"Look where you're going",
		"I'm watching you",
		"I've never seen you before",
		"Fuck off",
	}
	g.Hitpoints = 400
	g.Damage = 50
	g.Speed = 2
	g.ActioinPoints = 0.0
	g.SightRange = 5

	firstNode := StoryNode{Text: "Hi"}
	secondNode := StoryNode{Text: "Ask the principals, they are standing by the fire"}

	firstNode.addChoice("I need to find a necromancer", &secondNode)
	secondNode.addChoice("OK", &firstNode)

	g.StoryNode = &firstNode

	return g
}

func (npc *NPC) execOption(option string) {
	for _, choice := range npc.StoryNode.Choices {
		if choice.Option == trade {
			npc.IsTradeStarted = true
		} else if strings.ToLower(choice.Option) == strings.ToLower(option) {
			npc.StoryNode = choice.NextNode
		}
	}
}

func (npc *NPC) setNPCStatus(status NPCStatus) {
	npc.Status = status
}

func (npc *NPC) setInteractZones() {
	left := Pos{npc.Pos.X - 1, npc.Pos.Y}
	right := Pos{npc.Pos.X + 1, npc.Pos.Y}
	bottom := Pos{npc.Pos.X, npc.Pos.Y + 1}

	npc.InteractZones = []Pos{left, right, bottom}
}

func (npc *NPC) CloseDialog() {
	npc.IsDialogStarted = false
}

func (npc *NPC) CloseTrade() {
	npc.IsTradeStarted = false
}

func (npc *NPC) InRange(p Pos) bool {
	for _, i := range npc.InteractZones {
		if i == p {
			return true
		}
	}
	return false
}

func (npc *NPC) update(level *Level) {
	if npc.InRange(level.Player.Pos) {
		npc.setNPCStatus(Active)
		rand.Seed(time.Now().UTC().UnixNano())
		r := rand.Intn(len(npc.Messages))
		npc.CurrentMessage = npc.Messages[r]
	} else {
		npc.setNPCStatus(NotActive)
	}
}

func (npc *NPC) sellItem(sellItem *Item) {
	npc.Gold += sellItem.Price
	for i, item := range npc.ItemToSell {
		if item == sellItem {
			npc.ItemToSell = append(npc.ItemToSell[:i], npc.ItemToSell[i+1:]...)
			return
		}
	}
}

func (npc *NPC) buyItem(item *Item) {
	npc.Gold -= item.Price
	npc.ItemToSell = append(npc.ItemToSell, item)
}
