package game

type Monster struct {
	Character
}

var allMonsters = []int32{
	Skeleton,
	SkeletonWithSpit,
	Bat,
	Frog,
	Spider,
	SkeletonNecromancer,
	Rat,
	Ogre,
	FireElemental,
	RedDragon,
	WormHead,
	RedLizardWarrior,
	LavaWorm,
	IceDragon,
	IceMonster,
}

func newMonster(p Pos, tileID int32) *Monster {
	m := &Monster{}
	m.TileID = tileID
	m.Pos = p

	switch tileID {
	case Frog:
		m.Name = "Frog"
		m.Hitpoints = 100 // ! temp
		m.MaxHitpoints = 100
		m.Damage = 5 // ! temp
		m.Speed = 1
		m.ActioinPoints = 0.0
		m.SightRange = 6
	case Bat:
		m.Name = "Bat"
		m.Hitpoints = 100 // ! temp
		m.MaxHitpoints = 100
		m.Damage = 5 // ! temp
		m.Speed = 1
		m.ActioinPoints = 0.0
		m.SightRange = 6
	case Spider:
		m.Name = "Spider"
		m.Hitpoints = 100 // ! temp
		m.MaxHitpoints = 100
		m.Damage = 5 // ! temp
		m.Speed = 1
		m.ActioinPoints = 0.0
		m.SightRange = 6
	case Skeleton:
		m.Name = "Skeleton"
		m.Hitpoints = 100 // ! temp
		m.MaxHitpoints = 100
		m.Damage = 5 // ! temp
		m.Speed = 1
		m.ActioinPoints = 0.0
		m.SightRange = 6
	case SkeletonWithSpit:
		m.Name = "Skeleton with spit"
		m.Hitpoints = 100 // ! temp
		m.MaxHitpoints = 100
		m.Damage = 5 // ! temp
		m.Speed = 1
		m.ActioinPoints = 0.0
		m.SightRange = 6
	case Ogre:
		m.Name = "Ogre"
		m.Hitpoints = 100 // ! temp
		m.MaxHitpoints = 100
		m.Damage = 5 // ! temp
		m.Speed = 1
		m.ActioinPoints = 0.0
		m.SightRange = 6
	case Rat:
		m.Name = "Rat"
		m.Hitpoints = 100 // ! temp
		m.MaxHitpoints = 100
		m.Damage = 5 // ! temp
		m.Speed = 1
		m.ActioinPoints = 0.0
		m.SightRange = 6
	case SkeletonNecromancer:
		m.Name = "Skeleton necromancer"
		m.Hitpoints = 100 // ! temp
		m.MaxHitpoints = 100
		m.Damage = 5 // ! temp
		m.Speed = 1
		m.ActioinPoints = 0.0
		m.SightRange = 6
	case FireElemental:
		m.Name = "Fire elemental"
		m.Hitpoints = 200 // ! temp
		m.MaxHitpoints = 200
		m.Damage = 5 // ! temp
		m.Speed = 2
		m.ActioinPoints = 0.0
		m.SightRange = 6
	case RedDragon:
		m.Name = "Red dragon"
		m.Hitpoints = 200 // ! temp
		m.MaxHitpoints = 200
		m.Damage = 10 // ! temp
		m.Speed = 1
		m.ActioinPoints = 0.0
		m.SightRange = 6
	case WormHead:
		m.Name = "Worm Head"
		m.Hitpoints = 50 // ! temp
		m.MaxHitpoints = 50
		m.Damage = 40 // ! temp
		m.Speed = 0.5
		m.ActioinPoints = 0.0
		m.SightRange = 6
	case RedLizardWarrior:
		m.Name = "Red lizard warrior"
		m.Hitpoints = 30 // ! temp
		m.MaxHitpoints = 30
		m.Damage = 10 // ! temp
		m.Speed = 1
		m.ActioinPoints = 0.0
		m.SightRange = 6
	case LavaWorm:
		m.Name = "Lava worm"
		m.Hitpoints = 50 // ! temp
		m.MaxHitpoints = 50
		m.Damage = 50 // ! temp
		m.Speed = 0
		m.ActioinPoints = 0.0
		m.SightRange = 6
	case IceDragon:
		m.Name = "Ice dragon"
		m.Hitpoints = 50 // ! temp
		m.MaxHitpoints = 50
		m.Damage = 50 // ! temp
		m.Speed = 1
		m.ActioinPoints = 0.0
		m.SightRange = 6
	case IceMonster:
		m.Name = "Ice monster"
		m.Hitpoints = 50 // ! temp
		m.MaxHitpoints = 50
		m.Damage = 50 // ! temp
		m.Speed = 1
		m.ActioinPoints = 0.0
		m.SightRange = 6
	}

	return m
}

func (m *Monster) update(level *Level) {
	m.ActioinPoints += m.Speed
	playerPos := level.Player.Pos
	positions := level.astar(m.Pos, playerPos)

	if len(positions) == 0 {
		m.pass()
		return
	}

	apInt := int(m.ActioinPoints)
	moveIdx := 1

	for i := 0; i < apInt; i++ {
		if moveIdx < len(positions) && len(positions) <= m.SightRange {
			m.move(positions[moveIdx], level)
			moveIdx++
		}
		m.ActioinPoints--
	}

	m.UpdateHPBar()
}

func (m *Monster) move(to Pos, level *Level) {
	_, exists := level.Monsters[to]
	if !exists && to != level.Player.Pos {
		delete(level.Monsters, m.Pos)
		level.Monsters[to] = m
		m.Pos = to
		return
	}

	if to == level.Player.Pos {
		m.attack(&level.Player.Character, level)
		if level.Player.Hitpoints <= 0 {
			level.playerDieFromMonster(m)
		}
	}
}

func (m *Monster) die(level *Level) {
	level.Events.addEvent(level.Player.Name + " killed " + m.Name)
	level.Items[m.Pos] = append(level.Items[m.Pos], m.Inventory...)
	delete(level.Monsters, m.Pos)
}
