package game

import (
	"encoding/csv"
	"fmt"
	"io/ioutil"
	"math"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/veandco/go-sdl2/sdl"
)

type Tile struct {
	Main    int32
	Top     int32
	Visible bool
	Seen    bool
	Angle   float64
	Flip    sdl.RendererFlip
}

type LevelPos struct {
	Pos
	*Level
}

type Level struct {
	Map      [][60]*Tile
	Player   *Player
	Monsters map[Pos]*Monster
	NPCs     map[Pos]*NPC
	Portals  map[Pos]*LevelPos
	Torches  map[Pos]*Torch
	Items    map[Pos][]*Item
	Events   *EventsArray
	// Items     map[Pos]*Item
	// Debug     map[Pos]bool
	// LastEvent GameEvent
}

func (level *Level) initLevel(player *Player) {
	level.Player = player
	level.Map = make([][60]*Tile, 25)
	level.Portals = make(map[Pos]*LevelPos)
	level.Monsters = make(map[Pos]*Monster)
	level.Torches = make(map[Pos]*Torch)
	level.NPCs = make(map[Pos]*NPC)
	level.Items = make(map[Pos][]*Item)
	level.Events = newEventsArray(4)
}

func LoadLevels() (map[string]*Level, error) {
	// init player
	player := newPlayer()

	// create levels map
	levels := make(map[string]*Level)

	// read all dirs in levels folder and create levels
	levelDirs, err := ioutil.ReadDir("game/levels/")
	if err != nil {
		return nil, err
	}

	for _, levelDir := range levelDirs {
		levelsFiles, err := ioutil.ReadDir("game/levels/" + levelDir.Name())
		if err != nil {
			return nil, err
		}

		// ! level creation
		level := &Level{}
		level.initLevel(player)

		for _, file := range levelsFiles {
			levelFile, err := os.Open(fmt.Sprintf("game/levels/%s/%s", levelDir.Name(), file.Name()))
			if err != nil {
				return nil, err
			}

			csvReader := csv.NewReader(levelFile)
			csvReader.FieldsPerRecord = -1
			csvReader.TrimLeadingSpace = true

			rows, err := csvReader.ReadAll()
			if err != nil {
				return nil, err
			}

			for y, row := range rows {
				for x, col := range row {
					if strings.Contains(levelFile.Name(), "main") {
						// main tile layer
						colInt, err := strconv.ParseInt(col, 10, 32)
						if err != nil {
							return nil, err
						}
						level.Map[y][x] = &Tile{Main: int32(colInt), Flip: sdl.FLIP_NONE}
						addAngleAndFlipToMainTiles(level.Map[y][x], int32(colInt))
					} else if strings.Contains(levelFile.Name(), "top") {
						// top tile layer
						colInt, err := strconv.ParseInt(col, 10, 32)
						if err != nil {
							return nil, err
						}
						addAngleAndFlipToTopTiles(level.Map[y][x], int32(colInt))
						// set player position
						if int32(colInt) == player.TileID {
							p := Pos{X: x, Y: y}
							player.Pos = p
							player.LastCheckPoint = p
							level.Map[y][x].Top = -1
						}

						// Check if torch
						level.addTorch(Pos{X: x, Y: y}, int32(colInt))
						// Check if monster
						level.addMonster(Pos{X: x, Y: y}, int32(colInt))
						// Check if npc
						level.addNpc(Pos{X: x, Y: y}, int32(colInt))
						// Check if item
						level.addGroundItems(Pos{X: x, Y: y}, int32(colInt))
						// Chech if chest
						level.addChestItems(Pos{X: x, Y: y}, int32(colInt), 3)
					}
				}
			}
		}

		levels[levelDir.Name()] = level
	}

	return levels, nil
}

func addAngleAndFlipToTopTiles(tile *Tile, tileId int32) {
	switch tileId {
	case -1073741080:
		tile.Top = DungeonPortal
		tile.Angle = 180
	case -1073740910:
		tile.Top = SecretDoor
		tile.Angle = 180
	case -1610612636:
		tile.Top = ClosedDoor
		tile.Angle = 90
	case 1610613502:
		tile.Top = MagicPortal
		tile.Angle = 270
	case -1073741058:
		tile.Top = MagicPortal
		tile.Angle = 180
	case -1073741061:
		tile.Top = CavePortal
		tile.Angle = 180
	case 1610612836:
		tile.Top = ClosedDoor
		tile.Angle = 270
	case 1610613426:
		tile.Top = MagicGreenPortal
		tile.Angle = 270
	case -1073741055:
		tile.Top = ShopPortal
		tile.Angle = 180
	case -1610611969:
		tile.Top = BrokenPortal
		tile.Angle = 90
	case -1610611970:
		tile.Top = MagicPortal
		tile.Angle = 90
	case -2147479859:
		tile.Top = GhnomeCustomer
		tile.Flip = sdl.FLIP_HORIZONTAL
	default:
		tile.Top = tileId
		tile.Flip = sdl.FLIP_NONE
	}
}

func addAngleAndFlipToMainTiles(tile *Tile, tileId int32) {
	switch tileId {
	case -1610612620:
		tile.Main = LockedDoorSingle
		tile.Angle = 90
	case 1610612852:
		tile.Main = LockedDoorSingle
		tile.Angle = 270
	}
}

// if item on ground - add to items map
func (level *Level) addGroundItems(pos Pos, tileID int32) {
	if contains(allItems, tileID) {
		level.Items[pos] = append(level.Items[pos], newItem(pos, tileID))
		level.Map[pos.Y][pos.X].Top = -1
	}
}

func (level *Level) addChestItems(pos Pos, tileID int32, quantity int) {
	switch tileID {
	case ClosedChest:
		rand.Seed(time.Now().UTC().UnixNano())
		for i := 0; i < rand.Intn(quantity)+1; i++ {
			item := randomItemLVL1(pos)
			level.Items[pos] = append(level.Items[pos], item)
		}
	}
}

// if torch - add to torches map
func (level *Level) addTorch(pos Pos, tileId int32) {
	switch tileId {
	case TorchOff:
		level.Torches[pos] = newTorch(TorchOff, 3, pos, []Pos{})
		level.Map[pos.Y][pos.X].Top = -1
	case BigFire:
		level.Torches[pos] = newTorch(BigFire, 4, pos, []Pos{})
		level.Map[pos.Y][pos.X].Top = -1
	}
}

// if monster - add to monsters map
func (level *Level) addMonster(pos Pos, monsterId int32) {
	if contains(allMonsters, monsterId) {
		level.Monsters[pos] = newMonster(pos, monsterId)
		level.Map[pos.Y][pos.X].Top = -1
	}
}

// if npc - add to npcs map
func (level *Level) addNpc(pos Pos, npcID int32) {
	switch npcID {
	// ! Guards
	case TownGuardian:
		level.NPCs[pos] = newGuard(pos, "Town guardian", TownGuardian)
		level.Map[pos.Y][pos.X].Top = -1
	case GhnomeGuard1:
		level.NPCs[pos] = newGuard(pos, "Ghnome guard 1", GhnomeGuard1)
		level.Map[pos.Y][pos.X].Top = -1
	case GhnomeGuard2:
		level.NPCs[pos] = newGuard(pos, "Ghnome guard 2", GhnomeGuard2)
		level.Map[pos.Y][pos.X].Top = -1
		level.Map[pos.Y][pos.X].Flip = sdl.FLIP_HORIZONTAL
	// ! Traders
	case GhnomeTrader:
		level.NPCs[pos] = newTrader(pos, "Ghnome trader", GhnomeTrader)
		level.Map[pos.Y][pos.X].Top = -1
	case MagicTrader:
		level.NPCs[pos] = newTrader(pos, "Magic trader", MagicTrader)
		level.Map[pos.Y][pos.X].Top = -1
	case ArmorTrader:
		level.NPCs[pos] = newTrader(pos, "Armor trader", ArmorTrader)
		level.Map[pos.Y][pos.X].Top = -1
	// ! Regular
	case Pilgrim:
		level.NPCs[pos] = newNPC(pos, "Pilgrim", Pilgrim)
		level.Map[pos.Y][pos.X].Top = -1
	case Archmage:
		level.NPCs[pos] = newNPC(pos, "Archmage", Archmage)
		level.Map[pos.Y][pos.X].Top = -1
	case GnomeMage:
		level.NPCs[pos] = newNPC(pos, "Gnome mage", GnomeMage)
		level.Map[pos.Y][pos.X].Top = -1
	case DarkMage:
		level.NPCs[pos] = newNPC(pos, "Dark mage", DarkMage)
		level.Map[pos.Y][pos.X].Top = -1
		level.Map[pos.Y][pos.X].Flip = sdl.FLIP_HORIZONTAL
	case GhnomeCustomer:
		level.NPCs[pos] = newNPC(pos, "Ghnome customer", GhnomeCustomer)
		level.Map[pos.Y][pos.X].Top = -1
	}
}

func (level *Level) lineOfSight() {
	pos := level.Player.Pos
	dist := level.Player.SightRange

	for y := pos.Y - dist; y <= pos.Y+dist; y++ {
		for x := pos.X - dist; x <= pos.X+dist; x++ {
			xDelta := pos.X - x
			yDelta := pos.Y - y
			d := math.Sqrt(float64(xDelta*xDelta + yDelta*yDelta))
			if d <= float64(dist) {
				level.bresenham(pos, Pos{x, y})
			}
		}
	}
}

func (level *Level) moveItem(itemToMove *Item) {
	if len(level.Player.Inventory) == level.Player.InventoryMax {
		level.Events.addEvent(level.Player.Name + " can't take more items")
		return
	}
	items := level.Items[level.Player.Pos]
	for i, item := range items {
		if item == itemToMove {
			items = append(items[:i], items[i+1:]...)
			level.Items[level.Player.Pos] = items
			level.Player.Inventory = append(level.Player.Inventory, item)
			level.Events.addEvent(level.Player.Name + " picked up " + item.Name)
			return
		}
	}
	panic("no item")
}

func (level *Level) moveAllItems() []*Item {
	items := level.Items[level.Player.Pos]
	freeSpace := level.Player.InventoryMax - len(level.Player.Inventory)
	if freeSpace >= len(items) {
		level.Player.Inventory = append(level.Player.Inventory, items...)
		items = []*Item{}
	} else {
		level.Player.Inventory = append(level.Player.Inventory, items[:freeSpace]...)
		items = items[freeSpace:]
	}

	return items
}

func (level *Level) dropItem(itemToRemove *Item) {
	items := level.Player.Inventory
	for i, item := range items {
		if item == itemToRemove {
			level.Player.Inventory = append(level.Player.Inventory[:i], level.Player.Inventory[i+1:]...)
			level.Items[level.Player.Pos] = append(level.Items[level.Player.Pos], item)
			level.Events.addEvent(level.Player.Name + " Drop " + item.Name)
			return
		}
	}
	panic("no item")
}

// Check map borders
func (level *Level) inRange(pos Pos) bool {
	res := pos.X < len(level.Map[0]) && pos.Y < len(level.Map) && pos.X >= 0 && pos.Y >= 0
	return res
}

func (level *Level) canSeeThrough(pos Pos) bool {
	if level.inRange(pos) {
		tile := level.Map[pos.Y][pos.X]

		switch id := tile.Top; {
		case id == ClosedDoor: // closed door
			return false
		}

		switch id := tile.Main; {
		case id == LockedDoorSingle: // flags
			return false
		case id >= StoneWallsStart && id <= StoneWallsEnd: // stone wall
			return false
		case id >= LockedDoorStart && id <= LockedDoorEnd:
			return false
		}

		return true
	}
	return false
}

func (level *Level) canWalk(pos Pos) bool {
	if level.inRange(pos) {
		tile := level.Map[pos.Y][pos.X]

		switch id := tile.Top; {
		case id == ClosedChest || id == LockedChest: // chest
			return false
		case id == ClosedDoor: // closed door
			return false
		case id == MPShrine || id == HPShrine: // shrine
			return false
		case id == Flag: // flags
			return false
		case id == StoneGangoyle:
			return false
		case id == BunchOfSkulls:
			return false
		case id >= GravesStart && id <= GravesEnd:
			return false
		case id >= GoldStart && id <= GoldEnd:
			return false
		case id >= TreesStart && id <= TreesEnd: // trees
			return false
		case id >= SculpturesStart && id <= SculpturesEnd: // sculptures
			return false
		// check if portal
		case id >= PortalsStart && id <= PortalsEnd: // sculptures
			return true
		}

		switch id := tile.Main; {
		case id == LockedDoorSingle: // flags
			return false
		case id >= LockedDoorStart && id <= LockedDoorEnd:
			return false
		case id >= StoneWallsStart && id <= StoneWallsEnd: // stone wall
			return false
		case id >= BrickWallsStart && id <= BrickWallsEnd: // brick wall
			return false
		case id >= DirtWallsStart && id <= DirtWallsEnd: // dirt wall
			return false
		case id >= RuneWallsStart && id <= RuneWallsEnd: // rune wall
			return false
		case id >= WaterStart && id <= WaterEnd:
			return false
		}

		return true
	}
	return false
}

// When player dies from monster
func (level *Level) playerDieFromMonster(m *Monster) {
	p := level.Player
	p.takeOffAllItems()

	m.Inventory = append(m.Inventory, p.Inventory...)

	p.Inventory = []*Item{}
	p.Pos = p.LastCheckPoint

	p.Hitpoints = p.MaxHitpoints
	p.ManaPoints = p.MaxManaPoints

	p.UpdateHPBar()
	level.lineOfSight()

	level.Events.addEvent(m.Name + "killed " + p.Name)
}

func (level *Level) playerDieFromLava() {
	p := level.Player
	p.takeOffAllItems()

	level.Items[level.Player.Pos] = append(level.Items[level.Player.Pos], p.Inventory...)

	p.Inventory = []*Item{}
	p.Pos = p.LastCheckPoint

	p.Hitpoints = p.MaxHitpoints
	p.ManaPoints = p.MaxManaPoints

	p.UpdateHPBar()
	level.lineOfSight()

	level.Events.addEvent(p.Name + " die in lava")
}
