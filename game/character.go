package game

import (
	"fmt"
	"strconv"
)

type Characteristics struct {
	Damage       int
	Armor        int
	Intelligence float64
}

type Character struct {
	entity
	Characteristics

	Lvl  int
	Gold int

	MaxHitpoints  int
	MaxManaPoints int
	Hitpoints     int
	ManaPoints    int

	Speed         float64
	ActioinPoints float64
	SightRange    int

	Flip bool

	Inventory    []*Item
	InventoryMax int

	HPBarTileID int32

	Head, Weapon, Body, Cloak, Foot *Item
}

func (c *Character) CalcIntelligence() float64 {
	var intel float64

	intel += c.Intelligence

	if c.Head != nil {
		intel += c.Head.Intelligence
	}
	if c.Body != nil {
		intel += c.Body.Intelligence
	}
	if c.Cloak != nil {
		intel += c.Cloak.Intelligence
	}
	if c.Foot != nil {
		intel += c.Foot.Intelligence
	}
	if c.Weapon != nil {
		intel += c.Weapon.Intelligence
	}

	return intel
}

func (c *Character) CalcDamage() int {
	var dmg int

	dmg += c.Damage

	if c.Head != nil {
		dmg += c.Head.Damage
	}
	if c.Body != nil {
		dmg += c.Body.Damage
	}
	if c.Cloak != nil {
		dmg += c.Cloak.Damage
	}
	if c.Foot != nil {
		dmg += c.Foot.Damage
	}
	if c.Weapon != nil {
		dmg += c.Weapon.Damage
	}

	return dmg
}

func (c *Character) CalcArmor() int {
	var armor int

	armor += c.Armor

	if c.Head != nil {
		armor += c.Head.Armor
	}
	if c.Body != nil {
		armor += c.Body.Armor
	}
	if c.Cloak != nil {
		armor += c.Cloak.Armor
	}
	if c.Foot != nil {
		armor += c.Foot.Armor
	}
	if c.Weapon != nil {
		armor += c.Weapon.Armor
	}

	return armor
}

func (c *Character) UpdateHPBar() {
	hp80 := int(float64(c.MaxHitpoints) * .8)
	hp60 := int(float64(c.MaxHitpoints) * .6)
	hp40 := int(float64(c.MaxHitpoints) * .4)
	hp20 := int(float64(c.MaxHitpoints) * .2)
	hp10 := int(float64(c.MaxHitpoints) * .1)

	if c.Hitpoints > hp80 && c.Hitpoints <= c.MaxHitpoints {
		c.HPBarTileID = 0
	} else if c.Hitpoints > hp60 && c.Hitpoints <= hp80 {
		c.HPBarTileID = HPBar80
	} else if c.Hitpoints > hp40 && c.Hitpoints <= hp60 {
		c.HPBarTileID = HPBar60
	} else if c.Hitpoints > hp20 && c.Hitpoints <= hp40 {
		c.HPBarTileID = HPBar40
	} else if c.Hitpoints > hp10 && c.Hitpoints <= hp20 {
		c.HPBarTileID = HPBar20
	} else if c.Hitpoints <= hp10 {
		c.HPBarTileID = HPBar10
	}
}

func (c *Character) GetGold() int {
	return c.Gold
}

func (c *Character) addGold(g int) {
	c.Gold += g
}

func (c *Character) subtractGold(g int) {
	c.Gold -= g
}

func (c *Character) pass() {
	c.ActioinPoints -= c.Speed
}

func (c *Character) minusBonuses(item *Item) {
	c.Hitpoints -= item.HPBonus
	c.ManaPoints -= item.MPBonus
	c.MaxHitpoints -= item.HPBonus
	c.MaxManaPoints -= item.MPBonus
}

func (c *Character) plusBonuses(item *Item) {
	c.Hitpoints += item.HPBonus
	c.ManaPoints += item.MPBonus
	c.MaxHitpoints += item.HPBonus
	c.MaxManaPoints += item.MPBonus
}

func (c *Character) equip(itemToEquip *Item) {
	if c.Head != nil && itemToEquip.Type == Head {
		c.Inventory = append(c.Inventory, c.Head)
		c.minusBonuses(c.Head)
	} else if c.Weapon != nil && itemToEquip.Type == Weapon {
		c.Inventory = append(c.Inventory, c.Weapon)
		c.minusBonuses(c.Weapon)
	} else if c.Body != nil && itemToEquip.Type == Body {
		c.Inventory = append(c.Inventory, c.Body)
		c.minusBonuses(c.Body)
	} else if c.Foot != nil && itemToEquip.Type == Foot {
		c.Inventory = append(c.Inventory, c.Foot)
		c.minusBonuses(c.Foot)
	} else if c.Cloak != nil && itemToEquip.Type == Cloak {
		c.Inventory = append(c.Inventory, c.Cloak)
		c.minusBonuses(c.Cloak)
	}

	for i, item := range c.Inventory {
		if item == itemToEquip {
			c.Inventory = append(c.Inventory[:i], c.Inventory[i+1:]...)
			switch itemToEquip.Type {
			case Head:
				c.Head = itemToEquip
			case Weapon:
				c.Weapon = itemToEquip
			case Body:
				c.Body = itemToEquip
			case Foot:
				c.Foot = itemToEquip
			case Cloak:
				c.Cloak = itemToEquip
			}

			c.plusBonuses(item)

			return
		}
	}
}

func (c *Character) attack(enemy *Character, level *Level) {
	c.ActioinPoints--
	damage := c.CalcDamage() - enemy.CalcArmor()
	if damage <= 0 {
		damage = 0
	}
	enemy.Hitpoints -= damage

	level.Events.addEvent(c.Name + " Attacked " + enemy.Name + " with " + strconv.Itoa(damage))
	fmt.Println(c.Name + " Attacked " + enemy.Name + " with " + strconv.Itoa(damage))
}

func (c *Character) takeOffAllItems() {
	if c.Head != nil {
		c.Inventory = append(c.Inventory, c.Head)
		c.MaxHitpoints -= c.Head.HPBonus
		c.MaxManaPoints -= c.Head.MPBonus
		c.Head = nil
	}
	if c.Body != nil {
		c.Inventory = append(c.Inventory, c.Body)
		c.MaxHitpoints -= c.Body.HPBonus
		c.MaxManaPoints -= c.Body.MPBonus
		c.Body = nil
	}
	if c.Cloak != nil {
		c.Inventory = append(c.Inventory, c.Cloak)
		c.MaxHitpoints -= c.Cloak.HPBonus
		c.MaxManaPoints -= c.Cloak.MPBonus
		c.Cloak = nil
	}
	if c.Foot != nil {
		c.Inventory = append(c.Inventory, c.Foot)
		c.MaxHitpoints -= c.Foot.HPBonus
		c.MaxManaPoints -= c.Foot.MPBonus
		c.Foot = nil
	}
	if c.Weapon != nil {
		c.Inventory = append(c.Inventory, c.Weapon)
		c.MaxHitpoints -= c.Weapon.HPBonus
		c.MaxManaPoints -= c.Weapon.MPBonus
		c.Weapon = nil
	}
}
