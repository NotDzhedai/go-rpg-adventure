package game

// TODO Sounds

type inputType int

const (
	None inputType = iota
	Up
	Down
	Left
	Right
	QuitGame
	CloseWindow
	TakeItem
	TakeAllItems
	DropItem
	EquipItem
	MakeChoice
	PlayerSellItem
	PlayerBuyItem
)

type Pos struct {
	X, Y int
}

type Input struct {
	Type   inputType
	Item   *Item
	Choice *Choice
	Npc    *NPC
}

type Game struct {
	LevelChan    chan *Level
	InputChan    chan *Input
	Levels       map[string]*Level
	CurrentLevel *Level
}

func NewGame(startLvl string) (*Game, error) {
	levelChan := make(chan *Level)
	inputChan := make(chan *Input)

	levels, err := LoadLevels()
	if err != nil {
		return nil, err
	}

	game := &Game{levelChan, inputChan, levels, nil}

	// ! temp
	// ! SET LEVEL HERE
	// game.CurrentLevel = levels["1"]
	game.CurrentLevel = levels[startLvl]

	game.setLevelPortals()
	game.CurrentLevel.lineOfSight()

	return game, nil
}

// ? maybe read from file
func (game *Game) setLevelPortals() {
	// 1 >> 2
	game.Levels["1"].Portals[Pos{18, 0}] = &LevelPos{Pos: Pos{9, 23}, Level: game.Levels["2"]}
	// 2 >> 1
	game.Levels["2"].Portals[Pos{9, 24}] = &LevelPos{Pos: Pos{18, 1}, Level: game.Levels["1"]}
	// 2 >> 3
	game.Levels["2"].Portals[Pos{57, 0}] = &LevelPos{Pos: Pos{50, 23}, Level: game.Levels["3"]}
	// 3 >> 2
	game.Levels["3"].Portals[Pos{50, 24}] = &LevelPos{Pos: Pos{57, 1}, Level: game.Levels["2"]}
	// 3 >> 2 (town)
	game.Levels["2"].Portals[Pos{30, 14}] = &LevelPos{Pos: Pos{6, 17}, Level: game.Levels["3"]}
	// 2 (town) >> 3
	game.Levels["3"].Portals[Pos{7, 17}] = &LevelPos{Pos: Pos{30, 13}, Level: game.Levels["2"]}
	// 3.1 >> 3.2
	game.Levels["3"].Portals[Pos{50, 6}] = &LevelPos{Pos: Pos{28, 23}, Level: game.Levels["3"]}
	// 3.2 >> 3.1
	game.Levels["3"].Portals[Pos{28, 24}] = &LevelPos{Pos: Pos{51, 6}, Level: game.Levels["3"]}
	// 3.3 >> 3.4
	game.Levels["3"].Portals[Pos{12, 14}] = &LevelPos{Pos: Pos{6, 22}, Level: game.Levels["3"]}
	// 3.4 >> 3.3
	game.Levels["3"].Portals[Pos{6, 21}] = &LevelPos{Pos: Pos{13, 14}, Level: game.Levels["3"]}
	// 3.5 >> 3.6
	game.Levels["3"].Portals[Pos{3, 14}] = &LevelPos{Pos: Pos{4, 8}, Level: game.Levels["3"]}
	// 3.6 >> 3.5
	game.Levels["3"].Portals[Pos{4, 9}] = &LevelPos{Pos: Pos{3, 15}, Level: game.Levels["3"]}
	// 2 >> 4
	game.Levels["2"].Portals[Pos{19, 14}] = &LevelPos{Pos: Pos{1, 13}, Level: game.Levels["4"]}
	// 4 >> 2
	game.Levels["4"].Portals[Pos{0, 13}] = &LevelPos{Pos: Pos{20, 14}, Level: game.Levels["2"]}
	// 4 >> 5
	game.Levels["4"].Portals[Pos{53, 0}] = &LevelPos{Pos: Pos{1, 14}, Level: game.Levels["5"]}
	// 5 >> 4
	game.Levels["5"].Portals[Pos{0, 14}] = &LevelPos{Pos: Pos{53, 1}, Level: game.Levels["4"]}
}

func (game *Game) move(to Pos) {
	portal := game.CurrentLevel.Portals[to]
	if portal != nil {
		game.CurrentLevel = portal.Level
		game.CurrentLevel.Player.Pos = portal.Pos
		game.CurrentLevel.Player.setNewCheckpoint(portal.Pos)
	} else {
		game.CurrentLevel.Player.Pos = to

		if game.CurrentLevel.Map[to.Y][to.X].Main == Lava {
			game.CurrentLevel.Player.Hitpoints -= 10
		}

		if game.CurrentLevel.Player.Hitpoints <= 0 {
			game.CurrentLevel.playerDieFromLava()
		}

		for y, row := range game.CurrentLevel.Map {
			for x := range row {
				game.CurrentLevel.Map[y][x].Visible = false
			}
		}
	}
	game.CurrentLevel.lineOfSight()
}

func (game *Game) checkAndInteract(pos Pos) {
	if game.CurrentLevel.inRange(pos) {
		tile := game.CurrentLevel.Map[pos.Y][pos.X]
		switch tile.Top {
		case ClosedDoor:
			game.CurrentLevel.Map[pos.Y][pos.X].Top = OpenedDoor
			game.CurrentLevel.lineOfSight()
		case ClosedChest:
			game.CurrentLevel.Map[pos.Y][pos.X].Top = OpenedChest
		case HPShrine:
			game.CurrentLevel.Player.healHPInShrine()
			game.CurrentLevel.Events.addEvent(game.CurrentLevel.Player.Name + " restored all health")
		case MPShrine:
			game.CurrentLevel.Player.healMPInShrine()
			game.CurrentLevel.Events.addEvent(game.CurrentLevel.Player.Name + " restored all mana")
		case Gold1:
			game.CurrentLevel.Player.addGold(1)
			game.CurrentLevel.Events.addEvent(game.CurrentLevel.Player.Name + " picked up 1 gold")
			tile.Top = -1
		case Gold2:
			game.CurrentLevel.Player.addGold(2)
			game.CurrentLevel.Events.addEvent(game.CurrentLevel.Player.Name + " picked up 2 gold")
			tile.Top = -1
		case Gold3:
			game.CurrentLevel.Player.addGold(3)
			game.CurrentLevel.Events.addEvent(game.CurrentLevel.Player.Name + " picked up 3 gold")
			tile.Top = -1
		case Gold4:
			game.CurrentLevel.Player.addGold(4)
			game.CurrentLevel.Events.addEvent(game.CurrentLevel.Player.Name + " picked up 4 gold")
			tile.Top = -1
		case Gold5:
			game.CurrentLevel.Player.addGold(5)
			game.CurrentLevel.Events.addEvent(game.CurrentLevel.Player.Name + " picked up 5 gold")
			tile.Top = -1
		case Gold6:
			game.CurrentLevel.Player.addGold(6)
			game.CurrentLevel.Events.addEvent(game.CurrentLevel.Player.Name + " picked up 6 gold")
			tile.Top = -1
		case Gold7:
			game.CurrentLevel.Player.addGold(7)
			game.CurrentLevel.Events.addEvent(game.CurrentLevel.Player.Name + " picked up 7 gold")
			tile.Top = -1
		case Gold8:
			game.CurrentLevel.Player.addGold(8)
			game.CurrentLevel.Events.addEvent(game.CurrentLevel.Player.Name + " picked up 8 gold")
			tile.Top = -1
		case Gold9:
			game.CurrentLevel.Player.addGold(9)
			game.CurrentLevel.Events.addEvent(game.CurrentLevel.Player.Name + " picked up 9 gold")
			tile.Top = -1
		}
	}

}

func (game *Game) resolveMovement(pos Pos) {
	// if torch
	torch, tExists := game.CurrentLevel.Torches[pos]
	// if npc
	npc, nExists := game.CurrentLevel.NPCs[pos]
	// if monster
	monster, mExists := game.CurrentLevel.Monsters[pos]

	if mExists {
		game.CurrentLevel.Player.attack(&monster.Character, game.CurrentLevel)

		if monster.Hitpoints <= 0 {
			monster.die(game.CurrentLevel)
		}
	} else if tExists {
		torch.getTorchPosition(game.CurrentLevel)
	} else if nExists {
		if npc.InRange(game.CurrentLevel.Player.Pos) {
			npc.IsDialogStarted = true
		}
	} else if game.CurrentLevel.canWalk(pos) {
		game.move(pos)
	} else {
		game.checkAndInteract(pos)
	}

}

func (game *Game) handleInput(input *Input) {
	p := game.CurrentLevel.Player
	switch input.Type {
	case Up:
		game.resolveMovement(Pos{p.X, p.Y - 1})
	case Down:
		game.resolveMovement(Pos{p.X, p.Y + 1})
	case Left:
		game.resolveMovement(Pos{p.X - 1, p.Y})
		// p.Flip = false
	case Right:
		game.resolveMovement(Pos{p.X + 1, p.Y})
		// p.Flip = true
	case TakeItem:
		game.CurrentLevel.moveItem(input.Item)
	case DropItem:
		game.CurrentLevel.dropItem(input.Item)
	case TakeAllItems:
		game.CurrentLevel.Items[game.CurrentLevel.Player.Pos] = game.CurrentLevel.moveAllItems()
	case EquipItem:
		game.CurrentLevel.Player.equip(input.Item)
	case MakeChoice:
		input.Npc.execOption(input.Choice.Option)
	case PlayerBuyItem:
		if game.CurrentLevel.Player.Gold >= input.Item.Price &&
			len(p.Inventory) < p.InventoryMax {
			input.Npc.sellItem(input.Item)
			game.CurrentLevel.Player.buyItem(input.Item)
		}
	case PlayerSellItem:
		if input.Npc.Gold >= input.Item.Price &&
			len(input.Npc.ItemToSell) < input.Npc.InventoryMax {
			input.Npc.buyItem(input.Item)
			game.CurrentLevel.Player.sellItem(input.Item)
		}
	}
}

func (game *Game) Run() {
	game.LevelChan <- game.CurrentLevel

	for input := range game.InputChan {
		if input.Type == QuitGame {
			return
		}

		game.handleInput(input)
		game.CurrentLevel.Player.UpdateHPBar()

		// update torches
		for _, t := range game.CurrentLevel.Torches {
			t.update(game.CurrentLevel)
		}
		// update npcs
		for _, n := range game.CurrentLevel.NPCs {
			n.update(game.CurrentLevel)
		}
		// update monsters
		for _, m := range game.CurrentLevel.Monsters {
			m.update(game.CurrentLevel)
		}

		game.LevelChan <- game.CurrentLevel
	}
}
