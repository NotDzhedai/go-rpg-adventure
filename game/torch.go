package game

type Torch struct {
	TileID    int32
	TorchPos  Pos
	Radius    int
	Positions []Pos
}

func newTorch(tileID int32, radius int, torchPos Pos, positions []Pos) *Torch {
	return &Torch{TileID: tileID, Radius: radius, TorchPos: torchPos, Positions: positions}
}

func (t *Torch) update(level *Level) {
	if len(t.Positions) > 0 && t.TileID == TorchOff {
		t.TileID = TorchOn
	}
	for _, p := range t.Positions {
		level.Map[p.Y][p.X].Seen = true
		level.Map[p.Y][p.X].Visible = true
	}
}

func (t *Torch) getTorchPosition(level *Level) {
	positions := level.getNeighbors(t.TorchPos, level.canSeeThrough)

	for i := 0; i < t.Radius; i++ {
		for _, p := range positions {
			positions = append(positions, level.getNeighbors(p, level.canSeeThrough)...)
		}
	}

	positions = append(positions, t.TorchPos)
	t.Positions = positions
}
